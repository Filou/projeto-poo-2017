#pragma once
#include <iostream>
using namespace std;

class Pessoas
{
	int ID;
	string Nome;
	string Genero;
	string Cidade;

public:
	Pessoas(int ID, string Nome, string Genero, string Cidade);
	int GetID() { return ID; }
	string GetNome() { return Nome; }
	string GetGenero() { return Genero; }
	string GetCidade() { return Cidade; }
	~Pessoas();
};

