#pragma once
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
#include <string>
#include <iomanip> // duas decimais

//Class
#include "Universidade.h"
#include "Escola.h"
#include "Departamento.h"
#include "Cursos.h"
#include "Alunos.h"

namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ListarCursos
	/// </summary>
	public ref class ListarCursos : public System::Windows::Forms::Form
	{
	public:
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeCurso;
	public:
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Codigo;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeUniv;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeEsco;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeDepart;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::ComboBox^  CBCurso;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::ComboBox^  CBDepart;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ComboBox^  CBEsco;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ComboBox^  CBUniv;

	public: 
		list<Universidade *> *Universidades;
		list<Escola *> *Escolas;
		list<Departamento *> *Departamentos;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  TAlunos;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  MediaCurso;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	public:
		list<Cursos *> *Cursoss;
		ListarCursos(list<Universidade *> *Us, list<Escola *> *ES, list<Departamento *> *DS, list<Cursos *> *CS)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Universidades = Us;
			Escolas = ES;
			Departamentos = DS;
			Cursoss = CS;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ListarCursos()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	protected:






	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ListarCursos::typeid));
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->NomeCurso = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Codigo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeUniv = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeEsco = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeDepart = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->TAlunos = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->MediaCurso = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->CBCurso = (gcnew System::Windows::Forms::ComboBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->CBDepart = (gcnew System::Windows::Forms::ComboBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->CBEsco = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->CBUniv = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->menuStrip1->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(7) {
				this->NomeCurso,
					this->Codigo, this->NomeUniv, this->NomeEsco, this->NomeDepart, this->TAlunos, this->MediaCurso
			});
			this->dataGridView1->Location = System::Drawing::Point(27, 226);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->RowTemplate->Height = 24;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(1014, 368);
			this->dataGridView1->TabIndex = 6;
			// 
			// NomeCurso
			// 
			this->NomeCurso->HeaderText = L"Nome Curso";
			this->NomeCurso->Name = L"NomeCurso";
			this->NomeCurso->ReadOnly = true;
			// 
			// Codigo
			// 
			this->Codigo->HeaderText = L"Codigo";
			this->Codigo->Name = L"Codigo";
			this->Codigo->ReadOnly = true;
			// 
			// NomeUniv
			// 
			this->NomeUniv->HeaderText = L"Nome Universidade";
			this->NomeUniv->Name = L"NomeUniv";
			this->NomeUniv->ReadOnly = true;
			// 
			// NomeEsco
			// 
			this->NomeEsco->HeaderText = L"Nome Escola";
			this->NomeEsco->Name = L"NomeEsco";
			this->NomeEsco->ReadOnly = true;
			// 
			// NomeDepart
			// 
			this->NomeDepart->HeaderText = L"Nome Departamento";
			this->NomeDepart->Name = L"NomeDepart";
			this->NomeDepart->ReadOnly = true;
			// 
			// TAlunos
			// 
			this->TAlunos->HeaderText = L"Total Alunos";
			this->TAlunos->Name = L"TAlunos";
			this->TAlunos->ReadOnly = true;
			// 
			// MediaCurso
			// 
			this->MediaCurso->HeaderText = L"Media Curso";
			this->MediaCurso->Name = L"MediaCurso";
			this->MediaCurso->ReadOnly = true;
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1177, 28);
			this->menuStrip1->TabIndex = 7;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->exitToolStripMenuItem->Size = System::Drawing::Size(161, 26);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &ListarCursos::exitToolStripMenuItem_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->CBCurso);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->CBDepart);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->CBEsco);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->CBUniv);
			this->groupBox1->Controls->Add(this->dataGridView1);
			this->groupBox1->Location = System::Drawing::Point(54, 58);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(1060, 600);
			this->groupBox1->TabIndex = 8;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Listar Cursos";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(24, 153);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(1013, 17);
			this->label5->TabIndex = 17;
			this->label5->Text = resources->GetString(L"label5.Text");
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(47, 186);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(116, 17);
			this->label4->TabIndex = 16;
			this->label4->Text = L"Listar por Cursos";
			// 
			// CBCurso
			// 
			this->CBCurso->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBCurso->FormattingEnabled = true;
			this->CBCurso->Location = System::Drawing::Point(319, 183);
			this->CBCurso->Name = L"CBCurso";
			this->CBCurso->Size = System::Drawing::Size(686, 24);
			this->CBCurso->TabIndex = 15;
			this->CBCurso->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarCursos::CBCurso_SelectedIndexChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(47, 108);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(98, 17);
			this->label3->TabIndex = 14;
			this->label3->Text = L"Departamento";
			// 
			// CBDepart
			// 
			this->CBDepart->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBDepart->Enabled = false;
			this->CBDepart->FormattingEnabled = true;
			this->CBDepart->Location = System::Drawing::Point(319, 105);
			this->CBDepart->Name = L"CBDepart";
			this->CBDepart->Size = System::Drawing::Size(686, 24);
			this->CBDepart->TabIndex = 13;
			this->CBDepart->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarCursos::CBDepart_SelectedIndexChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(47, 78);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(50, 17);
			this->label2->TabIndex = 12;
			this->label2->Text = L"Escola";
			// 
			// CBEsco
			// 
			this->CBEsco->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBEsco->Enabled = false;
			this->CBEsco->FormattingEnabled = true;
			this->CBEsco->Location = System::Drawing::Point(319, 75);
			this->CBEsco->Name = L"CBEsco";
			this->CBEsco->Size = System::Drawing::Size(686, 24);
			this->CBEsco->TabIndex = 11;
			this->CBEsco->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarCursos::CBEsco_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(47, 48);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(91, 17);
			this->label1->TabIndex = 10;
			this->label1->Text = L"Universidade";
			// 
			// CBUniv
			// 
			this->CBUniv->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBUniv->FormattingEnabled = true;
			this->CBUniv->Location = System::Drawing::Point(319, 45);
			this->CBUniv->Name = L"CBUniv";
			this->CBUniv->Size = System::Drawing::Size(686, 24);
			this->CBUniv->TabIndex = 7;
			this->CBUniv->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarCursos::CBUniv_SelectedIndexChanged);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label7);
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->button3);
			this->groupBox2->Controls->Add(this->button4);
			this->groupBox2->Controls->Add(this->button2);
			this->groupBox2->Controls->Add(this->button1);
			this->groupBox2->Location = System::Drawing::Point(54, 670);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(1060, 100);
			this->groupBox2->TabIndex = 9;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Listagens";
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(512, 58);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(119, 36);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Menor";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &ListarCursos::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(387, 58);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(119, 36);
			this->button4->TabIndex = 2;
			this->button4->Text = L"Maior";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &ListarCursos::button4_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(68, 58);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(119, 36);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Maior";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &ListarCursos::button2_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(193, 58);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(119, 36);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Menor";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &ListarCursos::button1_Click);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(137, 29);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(110, 17);
			this->label6->TabIndex = 4;
			this->label6->Text = L"Listar por media";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(450, 29);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(120, 17);
			this->label7->TabIndex = 5;
			this->label7->Text = L"Listar total alunos";
			// 
			// ListarCursos
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1177, 782);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ListarCursos";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"ListarCursos";
			this->Load += gcnew System::EventHandler(this, &ListarCursos::ListarCursos_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
private: System::Void ListarCursos_Load(System::Object^  sender, System::EventArgs^  e) {
	CBUniv->Items->Add("--- Selecione uma Universidade ---");
	CBUniv->SelectedIndex = 0;
	
	
	for (list<Universidade*>::iterator itU = Universidades->begin(); itU != Universidades->end(); itU++) {
		System::String^ text = gcnew String(((*itU)->GetNome()).c_str());
		CBUniv->Items->Add(String::Format(text));
	}

	CBCurso->Items->Clear();
	CBCurso->Items->Add("--- Todos os Cursos ---");
	CBCurso->SelectedIndex = 0;
	for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
		System::String^ text = gcnew String(((*itC)->GetNome()).c_str());
		CBCurso->Items->Add(String::Format(text));
	}
	/************************** NESTE FOR � QUE ESTOU A FAZER A TAL VERIFICACAO **********************************
	for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
		int index = CBCurso->FindString(CBCurso->Text);
		System::String^ text = gcnew String(((*itC)->GetNome()).c_str());
		for(int i = this->CBCurso->SelectedIndex; i <= CBCurso->Items->Count; i++)
			if(text != CBCurso->Items->ToString)
				CBCurso->Items->Add(String::Format(text));
	}*/
	
	
}

private: System::Void CBUniv_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {

	if (CBUniv->SelectedIndex != 0 ) {
		CBCurso->SelectedIndex = 0;
		CBEsco->Enabled = TRUE;
		CBEsco->Items->Clear();
		CBEsco->Items->Add("--- Selecione uma Escola ---");
		CBEsco->SelectedIndex = 0;
		std::string NomeUniv = msclr::interop::marshal_as<std::string>(CBUniv->Text);
		for (list<Escola*>::iterator itE = Escolas->begin(); itE != Escolas->end(); itE++) {
			if ((*itE)->GetUniv()->GetNome() == NomeUniv) {
				System::String^ text = gcnew String(((*itE)->GetNome()).c_str());
				CBEsco->Items->Add(String::Format(text));
			}
		}
		dataGridView1->Rows->Clear();
		//std::string NomeUniv = msclr::interop::marshal_as<std::string>(CBUniv->Text);
		for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
			if ((((Escola *)((Departamento *)(*itC)->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str()) == NomeUniv && CBUniv->SelectedIndex != 0 && CBCurso->SelectedIndex == 0)
			{
				double Media = 0;
				list<Pessoas*> px = (*itC)->GetListAluno();
				for (list<Pessoas*>::iterator itP = px.begin(); itP != px.end(); itP++) {
					Media += ((Alunos *)(*itP))->GetMedia();
				}
				dataGridView1->Rows->Add(
					gcnew String(((*itC)->GetNome().c_str())),
					gcnew String(((*itC)->GetCod().c_str())),
					gcnew String((((Escola *)((Departamento *)(*itC)->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str())),
					gcnew String((((Departamento *)(*itC)->GetDepart())->GetEsco()->GetNome().c_str())),
					gcnew String(((*itC)->GetDepart()->GetNome().c_str())),
					gcnew Int32(Convert::ToInt32((*itC)->GetNAlunos())),
					gcnew Double(Convert::ToDouble(((*itC)->GetNAlunos() == 0 ? 0 : Media / (*itC)->GetNAlunos())))
				);
			}
		}
	}
	else {
		CBEsco->Items->Clear();
		CBEsco->Enabled = FALSE;
		CBDepart->Items->Clear();
		CBDepart->Enabled = FALSE;
	}	
}
private: System::Void CBEsco_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {;
		 if (CBEsco->SelectedIndex != 0) {
			 CBDepart->Enabled = TRUE;
			 CBDepart->Items->Clear();
			 CBDepart->Items->Add("--- Selecione um Departamento ---");
			 CBDepart->SelectedIndex = 0;
			 std::string NomeEsco = msclr::interop::marshal_as<std::string>(CBEsco->Text);
			 for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++) {
				 if ((*itD)->GetEsco()->GetNome() == NomeEsco) {
					 System::String^ text = gcnew String(((*itD)->GetNome()).c_str());
					 CBDepart->Items->Add(String::Format(text));
				 }
			 }
		 }
		 else {
			 CBDepart->Items->Clear();
			 CBDepart->Enabled = FALSE;
		 }

		 dataGridView1->Rows->Clear();
		 std::string NomeEsco = msclr::interop::marshal_as<std::string>(CBEsco->Text);
		 for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
			 if ((((Departamento *)(*itC)->GetDepart())->GetEsco()->GetNome().c_str()) == NomeEsco && CBEsco->SelectedIndex != 0)
			 {
				 double Media = 0;
				 list<Pessoas*> px = (*itC)->GetListAluno();
				 for (list<Pessoas*>::iterator itP = px.begin(); itP != px.end(); itP++) {
					 Media += ((Alunos *)(*itP))->GetMedia();
				 }
				 dataGridView1->Rows->Add(
					 gcnew String(((*itC)->GetNome().c_str())),
					 gcnew String(((*itC)->GetCod().c_str())),
					 gcnew String((((Escola *)((Departamento *)(*itC)->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str())),
					 gcnew String((((Departamento *)(*itC)->GetDepart())->GetEsco()->GetNome().c_str())),
					 gcnew String(((*itC)->GetDepart()->GetNome().c_str())),
					 gcnew Int32(Convert::ToInt32((*itC)->GetNAlunos())),
					 gcnew Double(Convert::ToDouble(((*itC)->GetNAlunos() == 0 ? 0 : Media / (*itC)->GetNAlunos())))

				 );
			 }
		 }
}
private: System::Void CBDepart_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	dataGridView1->Rows->Clear();
	std::string NomeDepart = msclr::interop::marshal_as<std::string>(CBDepart->Text);
	for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
		if (((*itC)->GetDepart()->GetNome().c_str()) == NomeDepart && CBDepart->SelectedIndex != 0)
		{
			double Media = 0;
			list<Pessoas*> px = (*itC)->GetListAluno();
			for (list<Pessoas*>::iterator itP = px.begin(); itP != px.end(); itP++) {
				Media += ((Alunos *)(*itP))->GetMedia();
			}
			dataGridView1->Rows->Add(
				gcnew String(((*itC)->GetNome().c_str())),
				gcnew String(((*itC)->GetCod().c_str())),
				gcnew String((((Escola *)((Departamento *)(*itC)->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str())),
				gcnew String((((Departamento *)(*itC)->GetDepart())->GetEsco()->GetNome().c_str())),
				gcnew String(((*itC)->GetDepart()->GetNome().c_str())),
				gcnew Int32(Convert::ToInt32((*itC)->GetNAlunos())),
				gcnew Double(Convert::ToDouble(((*itC)->GetNAlunos() == 0 ? 0 : Media / (*itC)->GetNAlunos())))

			);
		}
	}
}

private: System::Void CBCurso_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	if(CBUniv->SelectedIndex == 0) {
		if (CBCurso->SelectedIndex == 0) {
			dataGridView1->Rows->Clear();
			for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
				double Media = 0;
				list<Pessoas*> px = (*itC)->GetListAluno();
				for (list<Pessoas*>::iterator itP = px.begin(); itP != px.end(); itP++) {
					Media += ((Alunos *)(*itP))->GetMedia();
				}
				dataGridView1->Rows->Add(
					gcnew String(((*itC)->GetNome().c_str())),
					gcnew String(((*itC)->GetCod().c_str())),
					gcnew String((((Escola *)((Departamento *)(*itC)->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str())),
					gcnew String((((Departamento *)(*itC)->GetDepart())->GetEsco()->GetNome().c_str())),
					gcnew String(((*itC)->GetDepart()->GetNome().c_str())),
					gcnew Int32(Convert::ToInt32((*itC)->GetNAlunos())),
					gcnew Double(Convert::ToDouble(((*itC)->GetNAlunos() == 0 ? 0 : Media / (*itC)->GetNAlunos())))

				);
			}
		}
		else
		{
			
			std::string NomeCurso = msclr::interop::marshal_as<std::string>(CBCurso->Text);
			dataGridView1->Rows->Clear();
			for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
				if (((*itC)->GetNome().c_str()) == NomeCurso && CBUniv->SelectedIndex == 0)
				{
					double Media = 0;
					list<Pessoas*> px = (*itC)->GetListAluno();
					for (list<Pessoas*>::iterator itP = px.begin(); itP != px.end(); itP++) {
						Media += ((Alunos *)(*itP))->GetMedia();
					}
					dataGridView1->Rows->Add(
						gcnew String(((*itC)->GetNome().c_str())),
						gcnew String(((*itC)->GetCod().c_str())),
						gcnew String((((Escola *)((Departamento *)(*itC)->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str())),
						gcnew String((((Departamento *)(*itC)->GetDepart())->GetEsco()->GetNome().c_str())),
						gcnew String(((*itC)->GetDepart()->GetNome().c_str())),
						gcnew Int32(Convert::ToInt32((*itC)->GetNAlunos())),
						gcnew Double(Convert::ToDouble(((*itC)->GetNAlunos() == 0 ? 0 : Media / (*itC)->GetNAlunos())))

					);
				}
			}
		}
	
	}
	else
	{
		if (CBCurso->SelectedIndex != 0) {
			CBUniv->SelectedIndex = 0;
		}
			std::string NomeCurso = msclr::interop::marshal_as<std::string>(CBCurso->Text);
			dataGridView1->Rows->Clear();
			for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
				if (((*itC)->GetNome().c_str()) == NomeCurso && CBUniv->SelectedIndex == 0)
				{
					double Media = 0;
					list<Pessoas*> px = (*itC)->GetListAluno();
					for (list<Pessoas*>::iterator itP = px.begin(); itP != px.end(); itP++) {
						Media += ((Alunos *)(*itP))->GetMedia();
					}
					dataGridView1->Rows->Add(
						gcnew String(((*itC)->GetNome().c_str())),
						gcnew String(((*itC)->GetCod().c_str())),
						gcnew String((((Escola *)((Departamento *)(*itC)->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str())),
						gcnew String((((Departamento *)(*itC)->GetDepart())->GetEsco()->GetNome().c_str())),
						gcnew String(((*itC)->GetDepart()->GetNome().c_str())),
						gcnew Int32(Convert::ToInt32((*itC)->GetNAlunos())),
						gcnew Double(Convert::ToDouble(((*itC)->GetNAlunos() == 0 ? 0 : Media / (*itC)->GetNAlunos())))

					);
				}
			}
	}

}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	dataGridView1->Sort(dataGridView1->Columns["MediaCurso"], ListSortDirection::Ascending);
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	dataGridView1->Sort(dataGridView1->Columns["MediaCurso"], ListSortDirection::Descending);
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	dataGridView1->Sort(dataGridView1->Columns["TAlunos"], ListSortDirection::Ascending);
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	dataGridView1->Sort(dataGridView1->Columns["TAlunos"], ListSortDirection::Descending);
}
};
}
