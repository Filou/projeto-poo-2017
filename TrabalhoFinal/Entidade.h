#pragma once
#include <iostream>

using namespace std;

class Entidade
{
	int ID;
	string Nome;
	string Cod;
public:
	Entidade(int ID, string Nome, string Cod);
	int GetID() { return ID; }
	string GetNome() { return Nome;	};
	string GetCod() { return Cod; };
	//virtual void GravarXml(ofstream &File);
	/*void LerXml(ifstream &File);*/
	~Entidade();
};

