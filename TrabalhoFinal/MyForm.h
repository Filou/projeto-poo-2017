#pragma once
#include <list>
#include <fstream>
#include <sstream> 
#include <chrono>

//class
#include "Universidade.h" 
#include "Escola.h" 
#include "Departamento.h"
#include "Cursos.h"
#include "Alunos.h"
#include "Entidade.h"

//forms
#include "AdicionarUniversidade.h"
#include "ListarUniversidades.h"
#include "AdicionarEscola.h"
#include "ApagarUniversidade.h"
#include "ListarEscolas.h"
#include "about.h"
#include "AdicionarDepartamento.h"
#include "ListarDepartamento.h"
#include "AdicionarCursos.h"
#include "ListarCursos.h"
#include "AdicionarDisciplina.h"
#include "ListarDisciplina.h"
#include "AdicionarAluno.h"
#include "ListarAlunos.h"
#include "AdicionarDocente.h"
#include "FrmAuxiliar.h"
#include "ApagarAlunos.h"
#include "Manutencao.h"

namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Xml;


	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		list<Universidade *> *Universidades;
		list<Escola *> *Escolas;
		list<Departamento *> *Departamentos;
		list<Cursos *> *Cursoss;
		list<Disciplina *> *Disciplinas;
		list<Alunos *> *Alunoss;
		Departamento *DAtual;
	private: System::Windows::Forms::GroupBox^  groupBox4;

	private: System::Windows::Forms::Button^  button10;
	private: System::Windows::Forms::Button^  button11;
	private: System::Windows::Forms::GroupBox^  groupBox8;
	private: System::Windows::Forms::GroupBox^  groupBox9;
	private: System::Windows::Forms::GroupBox^  groupBox11;
	private: System::Windows::Forms::ComboBox^  CBEscola;

	private: System::Windows::Forms::ComboBox^  CBUniv;

	private: System::Windows::Forms::GroupBox^  groupBox10;
	private: System::Windows::Forms::ComboBox^  CBDepartamento;
	private: System::Windows::Forms::GroupBox^  groupBox12;
	private: System::Windows::Forms::ComboBox^  CBDisci;

	private: System::Windows::Forms::ComboBox^  CBCurso2;

	private: System::Windows::Forms::ComboBox^  CBUniv1;
	private: System::Windows::Forms::GroupBox^  groupBox13;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;

	private: System::Windows::Forms::Button^  button22;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label22;
	private: System::Windows::Forms::Label^  label21;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::Label^  label23;
	private: System::Windows::Forms::Button^  button23;
	private: System::Windows::Forms::ToolStripMenuItem^  universidadeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  escolasToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  dadosTesteToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  departamentosToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  cursosToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  disciplinasToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  alunosToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
	private: System::Windows::Forms::ToolStripMenuItem^  todosToolStripMenuItem;






	private: System::Windows::Forms::Button^  button12;
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Universidades = new list<Universidade*>();
			Escolas = new list<Escola*>();
			Departamentos = new list<Departamento*>();
			Cursoss = new list<Cursos*>();
			Disciplinas = new list<Disciplina*>();
			Alunoss = new list<Alunos*>();
			DAtual = NULL;
		}
		
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  loadToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Button^  button8;
	private: System::Windows::Forms::Button^  button9;




	private: System::Windows::Forms::GroupBox^  groupBox5;
	private: System::Windows::Forms::Button^  button13;
	private: System::Windows::Forms::Button^  button14;
	private: System::Windows::Forms::Button^  button15;
	private: System::Windows::Forms::GroupBox^  groupBox6;
	private: System::Windows::Forms::Button^  button16;
	private: System::Windows::Forms::Button^  button17;
	private: System::Windows::Forms::Button^  button18;
	private: System::Windows::Forms::GroupBox^  groupBox7;
	private: System::Windows::Forms::Button^  button19;
	private: System::Windows::Forms::Button^  button20;
	private: System::Windows::Forms::Button^  button21;
private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->universidadeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->escolasToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->departamentosToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->cursosToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->disciplinasToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->alunosToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->todosToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->dadosTesteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->button15 = (gcnew System::Windows::Forms::Button());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->button16 = (gcnew System::Windows::Forms::Button());
			this->button17 = (gcnew System::Windows::Forms::Button());
			this->button18 = (gcnew System::Windows::Forms::Button());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->button19 = (gcnew System::Windows::Forms::Button());
			this->button20 = (gcnew System::Windows::Forms::Button());
			this->button21 = (gcnew System::Windows::Forms::Button());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->groupBox8 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox9 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox11 = (gcnew System::Windows::Forms::GroupBox());
			this->CBDepartamento = (gcnew System::Windows::Forms::ComboBox());
			this->CBEscola = (gcnew System::Windows::Forms::ComboBox());
			this->CBUniv = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox10 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox12 = (gcnew System::Windows::Forms::GroupBox());
			this->CBDisci = (gcnew System::Windows::Forms::ComboBox());
			this->CBCurso2 = (gcnew System::Windows::Forms::ComboBox());
			this->CBUniv1 = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox13 = (gcnew System::Windows::Forms::GroupBox());
			this->button23 = (gcnew System::Windows::Forms::Button());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->button22 = (gcnew System::Windows::Forms::Button());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->menuStrip1->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox8->SuspendLayout();
			this->groupBox9->SuspendLayout();
			this->groupBox11->SuspendLayout();
			this->groupBox10->SuspendLayout();
			this->groupBox12->SuspendLayout();
			this->groupBox13->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->fileToolStripMenuItem,
					this->aboutToolStripMenuItem, this->exitToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1456, 28);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->loadToolStripMenuItem,
					this->saveToolStripMenuItem, this->dadosTesteToolStripMenuItem
			});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// loadToolStripMenuItem
			// 
			this->loadToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {
				this->universidadeToolStripMenuItem,
					this->escolasToolStripMenuItem, this->departamentosToolStripMenuItem, this->cursosToolStripMenuItem, this->disciplinasToolStripMenuItem,
					this->alunosToolStripMenuItem, this->toolStripSeparator1, this->todosToolStripMenuItem
			});
			this->loadToolStripMenuItem->Name = L"loadToolStripMenuItem";
			this->loadToolStripMenuItem->Size = System::Drawing::Size(165, 26);
			this->loadToolStripMenuItem->Text = L"Load";
			this->loadToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::loadToolStripMenuItem_Click);
			// 
			// universidadeToolStripMenuItem
			// 
			this->universidadeToolStripMenuItem->Name = L"universidadeToolStripMenuItem";
			this->universidadeToolStripMenuItem->Size = System::Drawing::Size(187, 26);
			this->universidadeToolStripMenuItem->Text = L"Universidades";
			this->universidadeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::universidadeToolStripMenuItem_Click);
			// 
			// escolasToolStripMenuItem
			// 
			this->escolasToolStripMenuItem->Name = L"escolasToolStripMenuItem";
			this->escolasToolStripMenuItem->Size = System::Drawing::Size(187, 26);
			this->escolasToolStripMenuItem->Text = L"Escolas";
			this->escolasToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::escolasToolStripMenuItem_Click);
			// 
			// departamentosToolStripMenuItem
			// 
			this->departamentosToolStripMenuItem->Name = L"departamentosToolStripMenuItem";
			this->departamentosToolStripMenuItem->Size = System::Drawing::Size(187, 26);
			this->departamentosToolStripMenuItem->Text = L"Departamentos";
			this->departamentosToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::departamentosToolStripMenuItem_Click);
			// 
			// cursosToolStripMenuItem
			// 
			this->cursosToolStripMenuItem->Name = L"cursosToolStripMenuItem";
			this->cursosToolStripMenuItem->Size = System::Drawing::Size(187, 26);
			this->cursosToolStripMenuItem->Text = L"Cursos";
			this->cursosToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::cursosToolStripMenuItem_Click);
			// 
			// disciplinasToolStripMenuItem
			// 
			this->disciplinasToolStripMenuItem->Name = L"disciplinasToolStripMenuItem";
			this->disciplinasToolStripMenuItem->Size = System::Drawing::Size(187, 26);
			this->disciplinasToolStripMenuItem->Text = L"Disciplinas";
			this->disciplinasToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::disciplinasToolStripMenuItem_Click);
			// 
			// alunosToolStripMenuItem
			// 
			this->alunosToolStripMenuItem->Name = L"alunosToolStripMenuItem";
			this->alunosToolStripMenuItem->Size = System::Drawing::Size(187, 26);
			this->alunosToolStripMenuItem->Text = L"Alunos";
			this->alunosToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::alunosToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(184, 6);
			// 
			// todosToolStripMenuItem
			// 
			this->todosToolStripMenuItem->Name = L"todosToolStripMenuItem";
			this->todosToolStripMenuItem->Size = System::Drawing::Size(187, 26);
			this->todosToolStripMenuItem->Text = L"Todos";
			this->todosToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::todosToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->Size = System::Drawing::Size(165, 26);
			this->saveToolStripMenuItem->Text = L"Save";
			this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::saveToolStripMenuItem_Click);
			// 
			// dadosTesteToolStripMenuItem
			// 
			this->dadosTesteToolStripMenuItem->Name = L"dadosTesteToolStripMenuItem";
			this->dadosTesteToolStripMenuItem->Size = System::Drawing::Size(165, 26);
			this->dadosTesteToolStripMenuItem->Text = L"Dados Teste";
			this->dadosTesteToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::dadosTesteToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(62, 24);
			this->aboutToolStripMenuItem->Text = L"About";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::aboutToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->exitToolStripMenuItem->Size = System::Drawing::Size(45, 24);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::exitToolStripMenuItem_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->button3);
			this->groupBox1->Controls->Add(this->button2);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Location = System::Drawing::Point(24, 34);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(428, 120);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Universidades";
			this->groupBox1->Enter += gcnew System::EventHandler(this, &MyForm::groupBox1_Enter);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(282, 39);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(123, 58);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Apagar";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(153, 39);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(123, 58);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Listar";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(24, 39);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(123, 58);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Adicionar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button4);
			this->groupBox2->Controls->Add(this->button5);
			this->groupBox2->Controls->Add(this->button6);
			this->groupBox2->Enabled = false;
			this->groupBox2->Location = System::Drawing::Point(458, 37);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(428, 120);
			this->groupBox2->TabIndex = 3;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Alunos";
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(282, 39);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(123, 58);
			this->button4->TabIndex = 2;
			this->button4->Text = L"Apagar";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(153, 39);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(123, 58);
			this->button5->TabIndex = 1;
			this->button5->Text = L"Listar";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(24, 39);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(123, 58);
			this->button6->TabIndex = 0;
			this->button6->Text = L"Adicionar";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &MyForm::button6_Click);
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->button7);
			this->groupBox3->Controls->Add(this->button8);
			this->groupBox3->Controls->Add(this->button9);
			this->groupBox3->Enabled = false;
			this->groupBox3->Location = System::Drawing::Point(892, 37);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(428, 120);
			this->groupBox3->TabIndex = 4;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Docentes";
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(282, 39);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(123, 58);
			this->button7->TabIndex = 2;
			this->button7->Text = L"Apagar";
			this->button7->UseVisualStyleBackColor = true;
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(153, 39);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(123, 58);
			this->button8->TabIndex = 1;
			this->button8->Text = L"Listar";
			this->button8->UseVisualStyleBackColor = true;
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(24, 39);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(123, 58);
			this->button9->TabIndex = 0;
			this->button9->Text = L"Adicionar";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &MyForm::button9_Click);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->button13);
			this->groupBox5->Controls->Add(this->button14);
			this->groupBox5->Controls->Add(this->button15);
			this->groupBox5->Enabled = false;
			this->groupBox5->Location = System::Drawing::Point(892, 60);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(428, 120);
			this->groupBox5->TabIndex = 5;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Disciplinas";
			// 
			// button13
			// 
			this->button13->Location = System::Drawing::Point(282, 39);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(123, 58);
			this->button13->TabIndex = 2;
			this->button13->Text = L"Apagar";
			this->button13->UseVisualStyleBackColor = true;
			this->button13->Click += gcnew System::EventHandler(this, &MyForm::button13_Click);
			// 
			// button14
			// 
			this->button14->Location = System::Drawing::Point(153, 39);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(123, 58);
			this->button14->TabIndex = 1;
			this->button14->Text = L"Listar";
			this->button14->UseVisualStyleBackColor = true;
			this->button14->Click += gcnew System::EventHandler(this, &MyForm::button14_Click);
			// 
			// button15
			// 
			this->button15->Location = System::Drawing::Point(24, 39);
			this->button15->Name = L"button15";
			this->button15->Size = System::Drawing::Size(123, 58);
			this->button15->TabIndex = 0;
			this->button15->Text = L"Adicionar";
			this->button15->UseVisualStyleBackColor = true;
			this->button15->Click += gcnew System::EventHandler(this, &MyForm::button15_Click);
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->button16);
			this->groupBox6->Controls->Add(this->button17);
			this->groupBox6->Controls->Add(this->button18);
			this->groupBox6->Enabled = false;
			this->groupBox6->Location = System::Drawing::Point(458, 60);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(428, 120);
			this->groupBox6->TabIndex = 6;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"Cursos";
			// 
			// button16
			// 
			this->button16->Location = System::Drawing::Point(282, 39);
			this->button16->Name = L"button16";
			this->button16->Size = System::Drawing::Size(123, 58);
			this->button16->TabIndex = 2;
			this->button16->Text = L"Apagar";
			this->button16->UseVisualStyleBackColor = true;
			this->button16->Click += gcnew System::EventHandler(this, &MyForm::button16_Click);
			// 
			// button17
			// 
			this->button17->Location = System::Drawing::Point(153, 39);
			this->button17->Name = L"button17";
			this->button17->Size = System::Drawing::Size(123, 58);
			this->button17->TabIndex = 1;
			this->button17->Text = L"Listar";
			this->button17->UseVisualStyleBackColor = true;
			this->button17->Click += gcnew System::EventHandler(this, &MyForm::button17_Click);
			// 
			// button18
			// 
			this->button18->Location = System::Drawing::Point(24, 39);
			this->button18->Name = L"button18";
			this->button18->Size = System::Drawing::Size(123, 58);
			this->button18->TabIndex = 0;
			this->button18->Text = L"Adicionar";
			this->button18->UseVisualStyleBackColor = true;
			this->button18->Click += gcnew System::EventHandler(this, &MyForm::button18_Click);
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->button19);
			this->groupBox7->Controls->Add(this->button20);
			this->groupBox7->Controls->Add(this->button21);
			this->groupBox7->Location = System::Drawing::Point(458, 34);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Size = System::Drawing::Size(428, 120);
			this->groupBox7->TabIndex = 4;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"Escola";
			// 
			// button19
			// 
			this->button19->Location = System::Drawing::Point(282, 39);
			this->button19->Name = L"button19";
			this->button19->Size = System::Drawing::Size(123, 58);
			this->button19->TabIndex = 2;
			this->button19->Text = L"Apagar";
			this->button19->UseVisualStyleBackColor = true;
			this->button19->Click += gcnew System::EventHandler(this, &MyForm::button19_Click);
			// 
			// button20
			// 
			this->button20->Location = System::Drawing::Point(153, 39);
			this->button20->Name = L"button20";
			this->button20->Size = System::Drawing::Size(123, 58);
			this->button20->TabIndex = 1;
			this->button20->Text = L"Listar";
			this->button20->UseVisualStyleBackColor = true;
			this->button20->Click += gcnew System::EventHandler(this, &MyForm::button20_Click);
			// 
			// button21
			// 
			this->button21->Location = System::Drawing::Point(24, 39);
			this->button21->Name = L"button21";
			this->button21->Size = System::Drawing::Size(123, 58);
			this->button21->TabIndex = 0;
			this->button21->Text = L"Adicionar";
			this->button21->UseVisualStyleBackColor = true;
			this->button21->Click += gcnew System::EventHandler(this, &MyForm::button21_Click);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->button10);
			this->groupBox4->Controls->Add(this->button11);
			this->groupBox4->Controls->Add(this->button12);
			this->groupBox4->Location = System::Drawing::Point(892, 34);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(428, 120);
			this->groupBox4->TabIndex = 7;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Departamentos";
			// 
			// button10
			// 
			this->button10->Location = System::Drawing::Point(282, 39);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(123, 58);
			this->button10->TabIndex = 2;
			this->button10->Text = L"Apagar";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &MyForm::button10_Click);
			// 
			// button11
			// 
			this->button11->Location = System::Drawing::Point(153, 39);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(123, 58);
			this->button11->TabIndex = 1;
			this->button11->Text = L"Listar";
			this->button11->UseVisualStyleBackColor = true;
			this->button11->Click += gcnew System::EventHandler(this, &MyForm::button11_Click);
			// 
			// button12
			// 
			this->button12->Location = System::Drawing::Point(24, 39);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(123, 58);
			this->button12->TabIndex = 0;
			this->button12->Text = L"Adicionar";
			this->button12->UseVisualStyleBackColor = true;
			this->button12->Click += gcnew System::EventHandler(this, &MyForm::button12_Click);
			// 
			// groupBox8
			// 
			this->groupBox8->Controls->Add(this->groupBox1);
			this->groupBox8->Controls->Add(this->groupBox4);
			this->groupBox8->Controls->Add(this->groupBox7);
			this->groupBox8->Location = System::Drawing::Point(81, 41);
			this->groupBox8->Name = L"groupBox8";
			this->groupBox8->Size = System::Drawing::Size(1347, 180);
			this->groupBox8->TabIndex = 8;
			this->groupBox8->TabStop = false;
			this->groupBox8->Text = L"Gest�o Recursos Principais";
			// 
			// groupBox9
			// 
			this->groupBox9->Controls->Add(this->groupBox11);
			this->groupBox9->Controls->Add(this->groupBox6);
			this->groupBox9->Controls->Add(this->groupBox5);
			this->groupBox9->Location = System::Drawing::Point(81, 240);
			this->groupBox9->Name = L"groupBox9";
			this->groupBox9->Size = System::Drawing::Size(1347, 218);
			this->groupBox9->TabIndex = 9;
			this->groupBox9->TabStop = false;
			this->groupBox9->Text = L"Gest�o de Cursos/Disciplinas";
			// 
			// groupBox11
			// 
			this->groupBox11->Controls->Add(this->CBDepartamento);
			this->groupBox11->Controls->Add(this->CBEscola);
			this->groupBox11->Controls->Add(this->CBUniv);
			this->groupBox11->Location = System::Drawing::Point(24, 60);
			this->groupBox11->Name = L"groupBox11";
			this->groupBox11->Size = System::Drawing::Size(428, 120);
			this->groupBox11->TabIndex = 7;
			this->groupBox11->TabStop = false;
			this->groupBox11->Text = L"Departamento a Editar";
			// 
			// CBDepartamento
			// 
			this->CBDepartamento->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBDepartamento->Enabled = false;
			this->CBDepartamento->FormattingEnabled = true;
			this->CBDepartamento->Location = System::Drawing::Point(101, 81);
			this->CBDepartamento->Name = L"CBDepartamento";
			this->CBDepartamento->Size = System::Drawing::Size(321, 24);
			this->CBDepartamento->TabIndex = 2;
			this->CBDepartamento->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::CBDepartamento_SelectedIndexChanged);
			this->CBDepartamento->Click += gcnew System::EventHandler(this, &MyForm::CBDepartamento_Click);
			// 
			// CBEscola
			// 
			this->CBEscola->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBEscola->Enabled = false;
			this->CBEscola->FormattingEnabled = true;
			this->CBEscola->Location = System::Drawing::Point(101, 51);
			this->CBEscola->Name = L"CBEscola";
			this->CBEscola->Size = System::Drawing::Size(321, 24);
			this->CBEscola->TabIndex = 1;
			this->CBEscola->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::CBEscola_SelectedIndexChanged);
			this->CBEscola->Click += gcnew System::EventHandler(this, &MyForm::CBEscola_Click);
			// 
			// CBUniv
			// 
			this->CBUniv->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBUniv->FormattingEnabled = true;
			this->CBUniv->Location = System::Drawing::Point(101, 21);
			this->CBUniv->Name = L"CBUniv";
			this->CBUniv->Size = System::Drawing::Size(321, 24);
			this->CBUniv->TabIndex = 0;
			this->CBUniv->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::CBUniv_SelectedIndexChanged);
			this->CBUniv->Click += gcnew System::EventHandler(this, &MyForm::CBUniv_Click);
			// 
			// groupBox10
			// 
			this->groupBox10->Controls->Add(this->groupBox12);
			this->groupBox10->Controls->Add(this->groupBox2);
			this->groupBox10->Controls->Add(this->groupBox3);
			this->groupBox10->Location = System::Drawing::Point(81, 464);
			this->groupBox10->Name = L"groupBox10";
			this->groupBox10->Size = System::Drawing::Size(1347, 178);
			this->groupBox10->TabIndex = 10;
			this->groupBox10->TabStop = false;
			this->groupBox10->Text = L"Gest�o de Alunos/Docentes";
			// 
			// groupBox12
			// 
			this->groupBox12->Controls->Add(this->CBDisci);
			this->groupBox12->Controls->Add(this->CBCurso2);
			this->groupBox12->Controls->Add(this->CBUniv1);
			this->groupBox12->Location = System::Drawing::Point(18, 37);
			this->groupBox12->Name = L"groupBox12";
			this->groupBox12->Size = System::Drawing::Size(428, 120);
			this->groupBox12->TabIndex = 8;
			this->groupBox12->TabStop = false;
			this->groupBox12->Text = L"Pessoa a Editar";
			// 
			// CBDisci
			// 
			this->CBDisci->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBDisci->Enabled = false;
			this->CBDisci->FormattingEnabled = true;
			this->CBDisci->Location = System::Drawing::Point(101, 81);
			this->CBDisci->Name = L"CBDisci";
			this->CBDisci->Size = System::Drawing::Size(321, 24);
			this->CBDisci->TabIndex = 2;
			this->CBDisci->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::CBDisci_SelectedIndexChanged);
			this->CBDisci->Click += gcnew System::EventHandler(this, &MyForm::CBDisci_Click);
			// 
			// CBCurso2
			// 
			this->CBCurso2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBCurso2->Enabled = false;
			this->CBCurso2->FormattingEnabled = true;
			this->CBCurso2->Location = System::Drawing::Point(101, 51);
			this->CBCurso2->Name = L"CBCurso2";
			this->CBCurso2->Size = System::Drawing::Size(321, 24);
			this->CBCurso2->TabIndex = 1;
			this->CBCurso2->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::CBCurso2_SelectedIndexChanged);
			this->CBCurso2->Click += gcnew System::EventHandler(this, &MyForm::CBCurso2_Click);
			// 
			// CBUniv1
			// 
			this->CBUniv1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBUniv1->FormattingEnabled = true;
			this->CBUniv1->Location = System::Drawing::Point(101, 21);
			this->CBUniv1->Name = L"CBUniv1";
			this->CBUniv1->Size = System::Drawing::Size(321, 24);
			this->CBUniv1->TabIndex = 0;
			this->CBUniv1->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::comboBox3_SelectedIndexChanged);
			this->CBUniv1->Click += gcnew System::EventHandler(this, &MyForm::CBUniv1_Click);
			// 
			// groupBox13
			// 
			this->groupBox13->Controls->Add(this->button23);
			this->groupBox13->Controls->Add(this->label23);
			this->groupBox13->Controls->Add(this->label22);
			this->groupBox13->Controls->Add(this->label21);
			this->groupBox13->Controls->Add(this->label20);
			this->groupBox13->Controls->Add(this->label19);
			this->groupBox13->Controls->Add(this->label18);
			this->groupBox13->Controls->Add(this->label17);
			this->groupBox13->Controls->Add(this->label16);
			this->groupBox13->Controls->Add(this->label15);
			this->groupBox13->Controls->Add(this->button22);
			this->groupBox13->Controls->Add(this->label13);
			this->groupBox13->Controls->Add(this->label14);
			this->groupBox13->Controls->Add(this->label11);
			this->groupBox13->Controls->Add(this->label12);
			this->groupBox13->Controls->Add(this->label9);
			this->groupBox13->Controls->Add(this->label10);
			this->groupBox13->Controls->Add(this->label7);
			this->groupBox13->Controls->Add(this->label8);
			this->groupBox13->Controls->Add(this->label5);
			this->groupBox13->Controls->Add(this->label6);
			this->groupBox13->Controls->Add(this->label3);
			this->groupBox13->Controls->Add(this->label4);
			this->groupBox13->Controls->Add(this->label2);
			this->groupBox13->Controls->Add(this->label1);
			this->groupBox13->Location = System::Drawing::Point(81, 648);
			this->groupBox13->Name = L"groupBox13";
			this->groupBox13->Size = System::Drawing::Size(1347, 101);
			this->groupBox13->TabIndex = 11;
			this->groupBox13->TabStop = false;
			this->groupBox13->Text = L"Estatisticas";
			// 
			// button23
			// 
			this->button23->Location = System::Drawing::Point(1197, 28);
			this->button23->Name = L"button23";
			this->button23->Size = System::Drawing::Size(123, 58);
			this->button23->TabIndex = 23;
			this->button23->Text = L"Apagar TUDO";
			this->button23->UseVisualStyleBackColor = true;
			this->button23->Click += gcnew System::EventHandler(this, &MyForm::button23_Click);
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(797, 28);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(11, 51);
			this->label23->TabIndex = 22;
			this->label23->Text = L"|\r\n|\r\n|";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(712, 28);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(11, 51);
			this->label22->TabIndex = 21;
			this->label22->Text = L"|\r\n|\r\n|";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(606, 28);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(11, 51);
			this->label21->TabIndex = 20;
			this->label21->Text = L"|\r\n|\r\n|";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(526, 28);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(11, 51);
			this->label20->TabIndex = 19;
			this->label20->Text = L"|\r\n|\r\n|";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(388, 28);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(11, 51);
			this->label19->TabIndex = 18;
			this->label19->Text = L"|\r\n|\r\n|";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(298, 28);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(11, 51);
			this->label18->TabIndex = 17;
			this->label18->Text = L"|\r\n|\r\n|";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(142, 28);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(11, 51);
			this->label17->TabIndex = 12;
			this->label17->Text = L"|\r\n|\r\n|";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label16->Location = System::Drawing::Point(88, 54);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(31, 32);
			this->label16->TabIndex = 16;
			this->label16->Text = L"0";
			this->label16->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(21, 28);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(98, 17);
			this->label15->TabIndex = 15;
			this->label15->Text = L"Memoria Total";
			// 
			// button22
			// 
			this->button22->Location = System::Drawing::Point(1045, 28);
			this->button22->Name = L"button22";
			this->button22->Size = System::Drawing::Size(123, 58);
			this->button22->TabIndex = 14;
			this->button22->Text = L"Mais dados";
			this->button22->UseVisualStyleBackColor = true;
			this->button22->Click += gcnew System::EventHandler(this, &MyForm::button22_Click);
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label13->Location = System::Drawing::Point(479, 51);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(31, 32);
			this->label13->TabIndex = 13;
			this->label13->Text = L"0";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(405, 28);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(105, 17);
			this->label14->TabIndex = 12;
			this->label14->Text = L"Departamentos";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label11->Location = System::Drawing::Point(855, 51);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(31, 32);
			this->label11->TabIndex = 11;
			this->label11->Text = L"0";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(818, 28);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(68, 17);
			this->label12->TabIndex = 10;
			this->label12->Text = L"Docentes";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label9->Location = System::Drawing::Point(756, 51);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(31, 32);
			this->label9->TabIndex = 9;
			this->label9->Text = L"0";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(736, 28);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(51, 17);
			this->label10->TabIndex = 8;
			this->label10->Text = L"Alunos";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label7->Location = System::Drawing::Point(668, 51);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(31, 32);
			this->label7->TabIndex = 7;
			this->label7->Text = L"0";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(624, 28);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(75, 17);
			this->label8->TabIndex = 6;
			this->label8->Text = L"Disciplinas";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(567, 51);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(31, 32);
			this->label5->TabIndex = 5;
			this->label5->Text = L"0";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(546, 28);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(52, 17);
			this->label6->TabIndex = 4;
			this->label6->Text = L"Cursos";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(350, 51);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(31, 32);
			this->label3->TabIndex = 3;
			this->label3->Text = L"0";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(324, 28);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(57, 17);
			this->label4->TabIndex = 2;
			this->label4->Text = L"Escolas";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(248, 51);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(31, 32);
			this->label2->TabIndex = 1;
			this->label2->Text = L"0";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(181, 28);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(98, 17);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Universidades";
			// 
			// timer1
			// 
			this->timer1->Tick += gcnew System::EventHandler(this, &MyForm::timer1_Tick);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1456, 804);
			this->Controls->Add(this->groupBox13);
			this->Controls->Add(this->groupBox10);
			this->Controls->Add(this->groupBox9);
			this->Controls->Add(this->groupBox8);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MyForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Gest�o de Universidade";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->groupBox3->ResumeLayout(false);
			this->groupBox5->ResumeLayout(false);
			this->groupBox6->ResumeLayout(false);
			this->groupBox7->ResumeLayout(false);
			this->groupBox4->ResumeLayout(false);
			this->groupBox8->ResumeLayout(false);
			this->groupBox9->ResumeLayout(false);
			this->groupBox11->ResumeLayout(false);
			this->groupBox10->ResumeLayout(false);
			this->groupBox12->ResumeLayout(false);
			this->groupBox13->ResumeLayout(false);
			this->groupBox13->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
		CBUniv->Items->Add("--- Selecione uma universidade ---");
		CBUniv->SelectedIndex = 0;
		CBUniv1->Items->Add("--- Selecione uma universidade ---");
		CBUniv1->SelectedIndex = 0;
		timer1->Start();
	}
	private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		Application::Exit();
	}
private: System::Void groupBox1_Enter(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew AdicionarUniversidade(Universidades);
	frm->Show();
}
private: System::Void Apagar(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew ListarUniversidades(Universidades);
	frm->Show();
}
private: System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew about;
	frm->Show();
}
private: System::Void button21_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew AdicionarEscola(Universidades, Escolas);
	frm->Show();
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew ApagarUniversidade(Universidades, Escolas);
	frm->Show();
}
private: System::Void button20_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew ListarEscolas(Escolas, Universidades);
	frm->Show();
}
private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew AdicionarDepartamento(Universidades, Escolas, Departamentos);
	frm->Show();
}
private: System::Void button11_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew ListarDepartamento(Departamentos, Universidades, Escolas);
	frm->Show();
}
private: System::Void CBUniv_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	CBEscola->Items->Clear();
	CBDepartamento->Items->Clear();
	if (CBUniv->SelectedIndex != 0) {
		CBEscola->Items->Add("--- Selecione uma Escola ---");
		CBEscola->SelectedIndex = 0;
		std::string NomeUniv = msclr::interop::marshal_as<std::string>(CBUniv->Text);
			for (list<Escola*>::iterator itE = Escolas->begin(); itE != Escolas->end(); itE++) {
				if (((*itE)->GetUniv()->GetNome()).c_str() == NomeUniv) {
					System::String^ text = gcnew String(((*itE)->GetNome()).c_str());
					CBEscola->Items->Add(String::Format(text));
				}
		}
		CBEscola->Enabled = TRUE;
	}
	else { CBEscola->Enabled = FALSE; CBDepartamento->Enabled = FALSE; groupBox6->Enabled = FALSE;
	groupBox5->Enabled = FALSE;
	}
	
}
private: System::Void CBEscola_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	CBDepartamento->Items->Clear();
	if (CBEscola->SelectedIndex != 0) {
		CBDepartamento->Items->Add("--- Selecione um Departamento ---");
		CBDepartamento->SelectedIndex = 0;
		std::string NomeEsco = msclr::interop::marshal_as<std::string>(CBEscola->Text);
		for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++) {
			if (((*itD)->GetEsco()->GetNome()).c_str() == NomeEsco) {
				System::String^ text = gcnew String(((*itD)->GetNome()).c_str());
				CBDepartamento->Items->Add(String::Format(text));
			}
		}
		CBDepartamento->Enabled = TRUE;
	}
	else { CBDepartamento->Enabled = FALSE; }
}
private: System::Void CBDepartamento_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	if (CBDepartamento->SelectedIndex != 0) {
		/*CBDepartamento->Items->Add("Selecione um Departamento");
		CBDepartamento->SelectedIndex = 0;*/
		std::string Nome = msclr::interop::marshal_as<std::string>(CBDepartamento->Text);
		for (list<Departamento*>::iterator itE = Departamentos->begin(); itE != Departamentos->end(); itE++) {
			if ((*itE)->GetNome() == Nome)
			{
				DAtual = *itE;
				groupBox6->Enabled = TRUE;
				groupBox5->Enabled = TRUE;
			}
		}
	}
	else
	{
		groupBox6->Enabled = FALSE;
		groupBox5->Enabled = FALSE;
	}
}
private: System::Void CBUniv_Click(System::Object^  sender, System::EventArgs^  e) {
	CBUniv->Items->Clear();
	CBUniv->Items->Add("--- Selecione uma universidade ---");
	CBUniv->SelectedIndex = 0;
	for (list<Universidade*>::iterator itE = Universidades->begin(); itE != Universidades->end(); itE++) {
			System::String^ text = gcnew String(((*itE)->GetNome()).c_str());
			CBUniv->Items->Add(String::Format(text));
	}
}
private: System::Void CBEscola_Click(System::Object^  sender, System::EventArgs^  e) {
	CBEscola->Items->Clear();
	CBEscola->Items->Add("--- Selecione uma Escola ---");
	CBEscola->SelectedIndex = 0;
	std::string NomeUniv = msclr::interop::marshal_as<std::string>(CBUniv->Text);
	for (list<Escola*>::iterator itE = Escolas->begin(); itE != Escolas->end(); itE++) {
		if (((*itE)->GetUniv()->GetNome()).c_str() == NomeUniv) {
			System::String^ text = gcnew String(((*itE)->GetNome()).c_str());
			CBEscola->Items->Add(String::Format(text));
		}
	}
}
private: System::Void CBDepartamento_Click(System::Object^  sender, System::EventArgs^  e) {
	CBDepartamento->Items->Clear();
	CBDepartamento->Items->Add("--- Selecione um Departamento ---");
	CBDepartamento->SelectedIndex = 0;
	std::string NomeEscola = msclr::interop::marshal_as<std::string>(CBEscola->Text);
	for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++) {
		if (((*itD)->GetEsco()->GetNome()).c_str() == NomeEscola) {
			System::String^ text = gcnew String(((*itD)->GetNome()).c_str());
			CBDepartamento->Items->Add(String::Format(text));
		}
	}
}

private: System::Void button18_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew AdicionarCursos(Universidades, Escolas, Departamentos, Cursoss);
	frm->Show();
}
private: System::Void button17_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew ListarCursos(Universidades, Escolas, Departamentos, Cursoss);
	frm->Show();
}
private: System::Void button15_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew AdicionarDisciplina(Universidades, Escolas, Departamentos, Cursoss, Disciplinas);
	frm->Show();
}
private: System::Void button14_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew ListarDisciplina(Universidades, Escolas, Departamentos, Cursoss, Disciplinas);
	frm->Show();
}
private: System::Void comboBox3_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	CBCurso2->Items->Clear();
	CBDisci->Items->Clear();
	if (CBUniv1->SelectedIndex != 0) {
		CBCurso2->Items->Add("--- Selecione um Curso ---");
		CBCurso2->SelectedIndex = 0;
		std::string NomeUniv = msclr::interop::marshal_as<std::string>(CBUniv1->Text);
		for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
			if ( ((Escola*)(*itC)->GetDepart()->GetEsco())->GetUniv()->GetNome() == NomeUniv) {
				System::String^ text = gcnew String(((*itC)->GetNome()).c_str());
				CBCurso2->Items->Add(String::Format(text));
			}
		}
		CBCurso2->Enabled = TRUE;
	}
	else {
		CBCurso2->Enabled = FALSE; CBDisci->Enabled = FALSE; groupBox2->Enabled = FALSE;
		groupBox3->Enabled = FALSE;
	}
}
private: System::Void CBUniv1_Click(System::Object^  sender, System::EventArgs^  e) {
	CBUniv1->Items->Clear();
	CBUniv1->Items->Add("--- Selecione uma universidade ---");
	CBUniv1->SelectedIndex = 0;
	for (list<Universidade*>::iterator itE = Universidades->begin(); itE != Universidades->end(); itE++) {
		System::String^ text = gcnew String(((*itE)->GetNome()).c_str());
		CBUniv1->Items->Add(String::Format(text));
	}
}
private: System::Void CBCurso2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	CBDisci->Items->Clear();
	if (CBCurso2->SelectedIndex != 0) {
		CBDisci->Items->Add("--- Selecione uma Disciplina	 ---");
		CBDisci->SelectedIndex = 0;
		std::string NomeCurso = msclr::interop::marshal_as<std::string>(CBCurso2->Text);
		for (list<Disciplina*>::iterator itDisc = Disciplinas->begin(); itDisc != Disciplinas->end(); itDisc++) {
			if ( (*itDisc)->GetCursos()->GetNome()  == NomeCurso) {
				System::String^ text = gcnew String(((*itDisc)->GetNome()).c_str());
				CBDisci->Items->Add(String::Format(text));
			}
		}
		CBDisci->Enabled = TRUE;
	}
	else { CBDisci->Enabled = FALSE; }
}
private: System::Void CBCurso2_Click(System::Object^  sender, System::EventArgs^  e) {
	CBCurso2->Items->Clear();
	CBCurso2->Items->Add("--- Selecione um Curso ---");
	CBCurso2->SelectedIndex = 0;
	std::string NomeUniv = msclr::interop::marshal_as<std::string>(CBUniv1->Text);
	for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
		if ( ((Escola*)((Departamento*)(*itC)->GetDepart())->GetEsco())->GetUniv()->GetNome()  == NomeUniv ) {
			System::String^ text = gcnew String(((*itC)->GetNome()).c_str());
			CBCurso2->Items->Add(String::Format(text));
		}
		
	}



}
private: System::Void CBDisci_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	if (CBDisci->SelectedIndex != 0) {
		/*CBDepartamento->Items->Add("Selecione um Departamento");
		CBDepartamento->SelectedIndex = 0;*/
		std::string NomeDisci = msclr::interop::marshal_as<std::string>(CBDisci->Text);
		for (list<Disciplina*>::iterator itDisci = Disciplinas->begin(); itDisci != Disciplinas->end(); itDisci++) {
			if ((*itDisci)->GetNome() == NomeDisci)
			{
				//DAtualDisci = *itDisci;
				groupBox2->Enabled = TRUE;
				groupBox3->Enabled = TRUE;
			}
		}
	}
	else
	{
		groupBox2->Enabled = FALSE;
		groupBox3->Enabled = FALSE;
	}
}
private: System::Void CBDisci_Click(System::Object^  sender, System::EventArgs^  e) {
	CBDisci->Items->Clear();
	CBDisci->Items->Add("--- Selecione uma Disciplina ---");
	CBDisci->SelectedIndex = 0;
	std::string NomeCurso = msclr::interop::marshal_as<std::string>(CBCurso2->Text);
	for (list<Disciplina*>::iterator itD = Disciplinas->begin(); itD != Disciplinas->end(); itD++) {
		if ( (*itD)->GetCursos()->GetNome() == NomeCurso) {
			System::String^ text = gcnew String(((*itD)->GetNome()).c_str());
			CBDisci->Items->Add(String::Format(text));
		}
	}
}
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew AdicionarAluno(Cursoss, Alunoss);
	frm->Show();
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew ListarAlunos(Cursoss, Alunoss);
	frm->Show();
}

			
/*private: System::VripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	
}*/
private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew AdicionarDocente(Disciplinas);
	frm->Show();
}

private: System::Void button22_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew FrmAuxiliar(Cursoss, Escolas, Alunoss);
	frm->Show();
}
private: int MemoriaOcupada(){
		int mem = 0;
		for (list<Universidade*>::iterator itU = Universidades->begin(); itU != Universidades->end(); itU++)
			mem += (*itU)->GetMemoria();
		for (list<Escola*>::iterator itE = Escolas->begin(); itE != Escolas->end(); itE++)
			mem += (*itE)->GetMemoria();
		for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++)
			mem += (*itD)->GetMemoria();
		for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
			mem += (*itC)->GetMemoria();
		}
		for (list<Disciplina*>::iterator itDisci = Disciplinas->begin(); itDisci != Disciplinas->end(); itDisci++)
			mem += (*itDisci)->GetMemoria();
		for (list<Alunos*>::iterator itA= Alunoss->begin(); itA != Alunoss->end(); itA++) {
			mem += (*itA)->GetMemoria();
		}

		 return mem;
		 //return sizeof(*this);
}

private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	label2->Text = Universidades->size().ToString();
	label3->Text = Escolas->size().ToString();
	label13->Text = Departamentos->size().ToString();
	label5->Text = Cursoss->size().ToString();
	label7->Text = Disciplinas->size().ToString();
	label9->Text = Alunoss->size().ToString();
	//label11->Text = Docentes->size().ToString();
	label16->Text = MemoriaOcupada().ToString();
	
}

private: System::Void button23_Click(System::Object^  sender, System::EventArgs^  e) {
	if (MessageBox::Show(
		"Deseja APAGAR ISTO TUDO ????\n",
		"Deseja MESMO?", MessageBoxButtons::YesNo,
		MessageBoxIcon::Question) == ::System::Windows::Forms::DialogResult::Yes)
	{
		Universidades->clear();
		Escolas->clear();
		Departamentos->clear();
		Cursoss->clear();
		Disciplinas->clear();
		Alunoss->clear();
		//Docentes->clear();
	}

}
private: System::Void loadToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	
}
private: System::Void universidadeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream Filehis;
	Filehis.open("dados/universidade.csv");
	auto start = std::chrono::high_resolution_clock::now();
	ifstream file("dados/universidade.xml");
	string line;
	// 4 vars
	int ID;
	string nomeU, localidade, codigo;

	while (getline(file, line))
	{
		size_t found = line.find("<");
		size_t found_2 = line.find(">");
		size_t found_3 = line.find("</");
		if (found != std::string::npos && found_2 != std::string::npos && found_3 != std::string::npos) {
			if (found_3 == 0) {
				//MessageBox::Show("aqui");
				string valor = line.substr(found_3 + 2, found_2 - found_3 - 2);
				//System::String^ text = gcnew String(valor.c_str());
				//MessageBox::Show(text);
				if (valor == "Univeridade") {
					// adicionar Univ
					Universidade *U = new Universidade(ID, nomeU, localidade, codigo);
					this->Universidades->push_back(U);
				}
			}
			else
			{
				string nome = line.substr(found + 1, found_2 - found - 1);
				string valor = line.substr(found_2 + 1, found_3 - found_2 - 1);
				//verificar os valores
				if (nome == "id")
					ID = atoi(valor.c_str());
				else if (nome == "Nome")
					nomeU = valor;
				else if (nome == "Localidade")
					localidade = valor;
				else if (nome == "Codigo")
					codigo = valor;
			}
		}
	}
	file.close();
	auto finish = std::chrono::high_resolution_clock::now();
	Filehis << "Univerisdade;" << std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count() << ";" << endl;
	Filehis.close();
}
private: System::Void escolasToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream Filehis;
	Filehis.open("dados/escola.csv");
	auto start = std::chrono::high_resolution_clock::now();
	ifstream file("dados/escola.xml");
	string line;
	// 7 vars
	int ID, IDuniv;
	string nomeE, regiao, codigo, website, logo;

	while (getline(file, line))
	{
		
		size_t found = line.find("<");
		size_t found_2 = line.find(">");
		size_t found_3 = line.find("</");

		if (found != std::string::npos && found_2 != std::string::npos && found_3 != std::string::npos) {
			if (found_3 == 0) {
				//MessageBox::Show("aqui");
				string valor = line.substr(found_3 + 2, found_2 - found_3 - 2);
				//System::String^ text = gcnew String(valor.c_str());
				//MessageBox::Show(text);
				if (valor == "Escola") {
					// adicionar Univ
					Entidade *U;
					for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
						if (((*it)->GetID()) == IDuniv)
						{
							U = (Entidade *)(*it);
							break;
						}
					}
					
					Escola *E = new Escola(ID, U, nomeE, regiao, codigo, website, logo);
					Escolas->push_back(E);
				}
			}
			else
			{
				string nome = line.substr(found + 1, found_2 - found - 1);
				string valor = line.substr(found_2 + 1, found_3 - found_2 - 1);
				//verificar os valores
				if (nome == "id")
					ID = atoi(valor.c_str());
				else if (nome == "idUniv")
					IDuniv = atoi(valor.c_str());
				else if (nome == "Nome")
					nomeE = valor;
				else if (nome == "Regiao")
					regiao = valor;
				else if (nome == "Codigo")
					codigo = valor;
				else if (nome == "Website")
					website = valor;
				else if (nome == "Logo")
					logo = valor;
			}
		}
		
	}
	auto finish = std::chrono::high_resolution_clock::now();
	Filehis << "Escolas;" << std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count() << ";" << endl;
	file.close();
	Filehis.close();
}

private: System::Void curspsoToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void saveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream File;
	File.open("dados/universidade.xml");
	File << "<LUnivesidades>" << endl;

	for (list<Universidade*>::iterator itU = Universidades->begin(); itU != Universidades->end(); itU++)
		((*itU))->GravarXml(File);
	File << "</LUnivesidades>" << endl;
	File.close();

	File.open("dados/escola.xml");
	File << "<LEscola>" << endl;
	for (list<Escola*>::iterator itE = Escolas->begin(); itE != Escolas->end(); itE++)
		((*itE))->GravarXml(File);
	File << "</LEscola>" << endl;
	File.close();

	File.open("dados/departamento.xml");
	File << "<LDepartamento>" << endl;
	for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++)
		((*itD))->GravarXml(File);
	File << "</LDepartamento>" << endl;
	File.close();

	File.open("dados/curso.xml");
	File << "<LCurso>" << endl;
	for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++)
		((*itC))->GravarXml(File);
	File << "</LCurso>" << endl;
	File.close();

	File.open("dados/disciplina.xml");
	File << "<LDisciplina>" << endl;

	for (list<Disciplina*>::iterator itD = Disciplinas->begin(); itD != Disciplinas->end(); itD++)
		((*itD))->GravarXml(File);
	File << "</LDisciplina>" << endl;
	File.close();

	File.open("dados/aluno.xml");
	File << "<LAluno>" << endl;
	for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++)
		((*itA))->GravarXml(File);
	File << "</LAluno>" << endl;
	File.close();

	/*File.open("dados/docente.xml");
	File << "<LDocente>" << endl;
	for (list<Docentes*>::iterator itU = Universidades->begin(); itU != Universidades->end(); itU++)
	((*itU))->GravarXml(File);
	File << "</LDocente>" << endl;
	File.close();*/
}

private: System::Void departamentosToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream Filehis;
	ifstream file("dados/departamento.xml");
	string line;
	// 5 vars
	int ID, IDEscola;
	string nomeD, codigo, logo;

	while (getline(file, line))
	{
		size_t found = line.find("<");
		size_t found_2 = line.find(">");
		size_t found_3 = line.find("</");
		if (found != std::string::npos && found_2 != std::string::npos && found_3 != std::string::npos) {
			if (found_3 == 0) {
				string valor = line.substr(found_3 + 2, found_2 - found_3 - 2);
				if (valor == "Departamento") {
					Entidade *U;
					for (list<Escola*>::iterator it = Escolas->begin(); it != Escolas->end(); it++) {
						if (((*it)->GetID()) == IDEscola)
						{
							U = (Entidade *)(*it);
							break;
						}
					}
					Departamento *E = new Departamento(ID, U,nomeD, codigo, logo);
					Departamentos->push_back(E);
				}
			}
			else
			{
				string nome = line.substr(found + 1, found_2 - found - 1);
				string valor = line.substr(found_2 + 1, found_3 - found_2 - 1);
				if (nome == "idEscola")
					IDEscola = atoi(valor.c_str());
				else if (nome == "id")
					ID = atoi(valor.c_str());
				else if (nome == "Nome")
					nomeD = valor;
				else if (nome == "Codigo")
					codigo = valor;
				else if (nome == "Logo")
					logo = valor;
			}
		}
	}
	file.close();
}
private: System::Void cursosToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream Filehis;
	//Filehis.open("dados/curso.csv");

	ifstream file("dados/curso.xml");
	string line;
	// 3 vars
	int IDDepart;
	string nomeC, codigo;

	while (getline(file, line))
	{
		//auto start = std::chrono::high_resolution_clock::now();
		size_t found = line.find("<");
		size_t found_2 = line.find(">");
		size_t found_3 = line.find("</");

		if (found != std::string::npos && found_2 != std::string::npos && found_3 != std::string::npos) {
			if (found_3 == 0) {
				
				string valor = line.substr(found_3 + 2, found_2 - found_3 - 2);
				
				if (valor == "Curso") {
					
					Departamento *U;
					for (list<Departamento*>::iterator it = Departamentos->begin(); it != Departamentos->end(); it++) {
						if (((*it)->GetID()) == IDDepart)
						{
							U = (*it);
							break;
						}
					}
				
					Cursos *E = new Cursos(codigo, nomeC, U);
					Cursoss->push_back(E);
				}
			}
			else
			{
				string nome = line.substr(found + 1, found_2 - found - 1);
				string valor = line.substr(found_2 + 1, found_3 - found_2 - 1);
				
				if (nome == "idDepart")
					IDDepart = atoi(valor.c_str());
				else if (nome == "Nome")
					nomeC = valor;
				else if (nome == "Codigo")
					codigo = valor;
			
			}
		}
		//auto finish = std::chrono::high_resolution_clock::now();
		//Filehis << "Cursos;" << std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count() << ";" << endl;
	}
	file.close();
	//Filehis.close();
}

private: System::Void disciplinasToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream Filehis;
	//Filehis.open("dados/disciplina.csv");

	ifstream file("dados/disciplina.xml");
	string line;
	// 4 vars
	int ects;
	string nomeC, codigo, Nome;

	while (getline(file, line))
	{
		//auto start = std::chrono::high_resolution_clock::now();
		size_t found = line.find("<");
		size_t found_2 = line.find(">");
		size_t found_3 = line.find("</");

		if (found != std::string::npos && found_2 != std::string::npos && found_3 != std::string::npos) {
			if (found_3 == 0) {
				string valor = line.substr(found_3 + 2, found_2 - found_3 - 2);
				if (valor == "Disciplina") {
					Cursos *C;
					for (list<Cursos *>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
						if (((*itC)->GetNome()).c_str() == nomeC)
						{
							C = (*itC);
							break;
						}
					}
					Disciplina *D = new Disciplina(C, codigo, Nome, ects);
					Disciplinas->push_back(D);
				}
			}
			else
			{
				string nome = line.substr(found + 1, found_2 - found - 1);
				string valor = line.substr(found_2 + 1, found_3 - found_2 - 1);
				
				if (nome == "Curso")
					nomeC = valor;
				else if (nome == "Nome")
					Nome = valor;
				else if (nome == "ECTS")
					ects = atoi(valor.c_str());
				else if (nome == "Codigo")
					codigo = valor;

			}
		}
		//auto finish = std::chrono::high_resolution_clock::now();
		//Filehis << "Disciplina;" << std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count() << ";" << endl;
	}
	file.close();
	//Filehis.close();
}

private: System::Void alunosToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream Filehis;
	//Filehis.open("dados/aluno.csv");

	ifstream file("dados/aluno.xml");
	string line;
	// 8 vars
	int id ,media, nmeca;
	string nomeC, codigo, nomeA, genero, cidade;

	while (getline(file, line))
	{
		//auto start = std::chrono::high_resolution_clock::now();
		size_t found = line.find("<");
		size_t found_2 = line.find(">");
		size_t found_3 = line.find("</");

		if (found != std::string::npos && found_2 != std::string::npos && found_3 != std::string::npos) {
			if (found_3 == 0) {
				string valor = line.substr(found_3 + 2, found_2 - found_3 - 2);
				if (valor == "Aluno") {
					Cursos *C;

					for (list<Cursos*>::iterator itD = Cursoss->begin(); itD != Cursoss->end(); itD++) {
						if (((*itD)->GetNome()).c_str() == nomeC)
						{
							C = (*itD);
							break;
						}
					}
					Alunos *A = new Alunos(id, nomeA, genero, cidade, nmeca, media, C);
					Alunoss->push_back(A);
					C->AddAluno(A);
				}
			}
			else
			{
				string nome = line.substr(found + 1, found_2 - found - 1);
				string valor = line.substr(found_2 + 1, found_3 - found_2 - 1);
				//verificar os valores
				if (nome == "Curso")
					nomeC = valor;
				else if (nome == "ID")
					id = atoi(valor.c_str());
				else if (nome == "Nome")
					nomeA = valor;
				else if (nome == "Genero")
					genero = valor;
				else if (nome == "Cidade")
					cidade = valor;
				else if (nome == "NMeca")
					nmeca = atoi(valor.c_str());
				else if (nome == "Media")
					media = atoi(valor.c_str());

			}
		}
		//auto finish = std::chrono::high_resolution_clock::now();
		//Filehis << "Aluno;" << std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count() << ";" << endl;
	}
	file.close();
	//Filehis.close();
}

private: System::Void dadosTesteToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream File;
	int Dados = 1000;
	int random = 100;
	File.open("dados/universidade.xml");
	File << "<LUnivesidades>" << endl;
	for (int i = 0; i < Dados; i++)
	{
		int v1 = rand() % random;
		string String = static_cast<ostringstream*>(&(ostringstream() << v1))->str();
		Universidade *U = new Universidade(1, String, String, String);
		U->GravarXml(File);
	}
	File << "</LUnivesidades>" << endl;
	File.close();

	File.open("dados/escola.xml");
	File << "<LEscola>" << endl;
	for (int i = 0; i < Dados; i++)
	{
		int v1 = rand() % random;
		string String = static_cast<ostringstream*>(&(ostringstream() << v1))->str();
		Entidade *U = new Entidade(1, String, String);
		Escola *E = new Escola(i, U, String, String, String, String, String);
		E->GravarXml(File);
	}
	File << "</LEscola>" << endl;
	File.close();

	File.open("dados/departamento.xml");
	File << "<LDepartamento>" << endl;
	for (int i = 0; i < Dados; i++)
	{
		int v1 = rand() % random;
		string String = static_cast<ostringstream*>(&(ostringstream() << v1))->str();
		Entidade *U = new Entidade(1, String, String);
		Departamento *E = new Departamento(1, U, String, String, String);
		E->GravarXml(File);
	}
	File << "</LDepartamento>" << endl;
	File.close();

	File.open("dados/curso.xml");
	File << "<LCurso>" << endl;
	for (int i = 0; i < Dados; i++)
	{
		int v1 = rand() % random;
		string String = static_cast<ostringstream*>(&(ostringstream() << v1))->str();
		Entidade *En = new Entidade(1, String, String);
		Departamento *U = new Departamento(1, En, String, String, String);
		Cursos *E = new Cursos(String, "teste", U);
		E->GravarXml(File);
	}
	File << "</LCurso>" << endl;
	File.close();

	File.open("dados/disciplina.xml");
	File << "<LDisciplina>" << endl;
	for (int i = 0; i < Dados; i++)
	{
		int v1 = rand() % random;
		string String = static_cast<ostringstream*>(&(ostringstream() << v1))->str();
		Cursos *U = new Cursos(String, "teste", NULL);
		//Cursoss->begin();
		Disciplina *E = new Disciplina(U, String, String, v1);
		E->GravarXml(File);
	}
	File << "</LDisciplina>" << endl;
	File.close();

	File.open("dados/aluno.xml");
	File << "<LAluno>" << endl;
	for (int i = 0; i < Dados; i++)
	{
		int v1 = rand() % random;
		string String = static_cast<ostringstream*>(&(ostringstream() << v1))->str();
		Cursos *C = new Cursos(String, "teste", NULL);
		Alunos *A = new Alunos(i, String, String, String, v1, v1, C);
		C->AddAluno(A);
		A->GravarXml(File);
	}
	File << "</LAluno>" << endl;
	File.close();

}
private: System::Void todosToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	ofstream Filehis;
	Filehis.open("dados/totalPrograma.csv", std::ofstream::out | std::ofstream::app);
	auto start = std::chrono::high_resolution_clock::now();

	//Executa as fun��es todas de leitura de dados XML
	universidadeToolStripMenuItem_Click(sender, e);
	escolasToolStripMenuItem_Click(sender, e);
	departamentosToolStripMenuItem_Click(sender, e);
	cursosToolStripMenuItem_Click(sender, e);
	disciplinasToolStripMenuItem_Click(sender, e);
	alunosToolStripMenuItem_Click(sender, e);
	if (Universidades->begin() == Universidades->end()) {
		MessageBox::Show("Sem dados para Carregar :(");
		return;
	}
	MessageBox::Show("Dados Carregados com Sucesso! :D");

	auto finish = std::chrono::high_resolution_clock::now();
	Filehis << "100000;" << std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start).count() << ";" << endl;
	Filehis.close();
}



private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew ApagarAlunos(Cursoss,Alunoss);
	frm->Show();
}
private: System::Void button16_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew Manutencao();
	frm->Show();
}
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew Manutencao();
	frm->Show();
}
private: System::Void button19_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew Manutencao();
	frm->Show();
}
private: System::Void button13_Click(System::Object^  sender, System::EventArgs^  e) {
	Form ^frm = gcnew Manutencao();
	frm->Show();
}
};
}
