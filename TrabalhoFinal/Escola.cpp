#include "Escola.h"



Escola::Escola(int ID, Entidade * Univ, string Nome, string regiao, string codigo, string website, string logo)
	:Entidade (ID, Nome, codigo)
{
	Escola::Regiao = regiao;
	Escola::Website = website;
	Escola::Logo = logo;
	Escola::Univ = Univ;
}

void Escola::GravarXml(ofstream & File)
{
	File << "<Escola>" << endl;
	File << "<idUniv>" << GetUniv()->GetID() << "</idUniv>" << endl;
	File << "<id>" << GetID() << "</id>" << endl;
	File << "<Nome>" << GetNome() << "</Nome>" << endl;
	File << "<Regiao>" << GetReg() << "</Regiao>" << endl;
	File << "<Codigo>" << GetCod() << "</Codigo>" << endl;
	File << "<Website>" << GetWeb() << "</Website>" << endl;
	File << "<Logo>" << GetLogo() << "</Logo>" << endl;
	File << "</Escola>" << endl;
}

Escola::~Escola()
{
}
