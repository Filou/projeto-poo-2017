#pragma once
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
#include <string>
//Class
#include "Departamento.h"

namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ListarDepartamento
	/// </summary>
	public ref class ListarDepartamento : public System::Windows::Forms::Form
	{
	public:
		list<Departamento *> *Departamentos;
		list<Escola *> *Escolas;
		list<Universidade *> *Universidades;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::DataGridView^  dataGridView1;







	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ID;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeUniv;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeEsco;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Nome;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Logo;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Codigo;
	public:
	private: System::Windows::Forms::ComboBox^  comboBox2;
	public:
		ListarDepartamento(list<Departamento *> *Ds, list<Universidade *> *Us, list<Escola *> *Es)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Departamentos = Ds;
			Escolas = Es;
			Universidades = Us;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ListarDepartamento()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->ID = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeUniv = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeEsco = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Nome = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Logo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Codigo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->menuStrip1->SuspendLayout();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// comboBox1
			// 
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(313, 38);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(470, 24);
			this->comboBox1->TabIndex = 1;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarDepartamento::comboBox1_SelectedIndexChanged);
			// 
			// comboBox2
			// 
			this->comboBox2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Location = System::Drawing::Point(313, 68);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(470, 24);
			this->comboBox2->TabIndex = 2;
			this->comboBox2->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarDepartamento::comboBox2_SelectedIndexChanged);
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1127, 28);
			this->menuStrip1->TabIndex = 3;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->exitToolStripMenuItem->Size = System::Drawing::Size(181, 26);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &ListarDepartamento::exitToolStripMenuItem_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->dataGridView1);
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Controls->Add(this->comboBox2);
			this->groupBox1->Location = System::Drawing::Point(27, 50);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(1090, 493);
			this->groupBox1->TabIndex = 4;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"groupBox1";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(15, 71);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(54, 17);
			this->label2->TabIndex = 7;
			this->label2->Text = L"Escola:";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(15, 41);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(95, 17);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Universidade:";
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
				this->ID, this->NomeUniv,
					this->NomeEsco, this->Nome, this->Logo, this->Codigo
			});
			this->dataGridView1->Location = System::Drawing::Point(18, 108);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->RowTemplate->Height = 24;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(1060, 379);
			this->dataGridView1->TabIndex = 5;
			// 
			// ID
			// 
			this->ID->HeaderText = L"Identificador";
			this->ID->Name = L"ID";
			this->ID->ReadOnly = true;
			// 
			// NomeUniv
			// 
			this->NomeUniv->HeaderText = L"Nome Universidade";
			this->NomeUniv->Name = L"NomeUniv";
			this->NomeUniv->ReadOnly = true;
			// 
			// NomeEsco
			// 
			this->NomeEsco->HeaderText = L"Nome Escola";
			this->NomeEsco->Name = L"NomeEsco";
			this->NomeEsco->ReadOnly = true;
			// 
			// Nome
			// 
			this->Nome->HeaderText = L"Nome Departamento";
			this->Nome->Name = L"Nome";
			this->Nome->ReadOnly = true;
			// 
			// Logo
			// 
			this->Logo->HeaderText = L"Logo";
			this->Logo->Name = L"Logo";
			this->Logo->ReadOnly = true;
			// 
			// Codigo
			// 
			this->Codigo->HeaderText = L"Codigo";
			this->Codigo->Name = L"Codigo";
			this->Codigo->ReadOnly = true;
			// 
			// ListarDepartamento
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1127, 566);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ListarDepartamento";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"ListarDepartamento";
			this->Load += gcnew System::EventHandler(this, &ListarDepartamento::ListarDepartamento_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ListarDepartamento_Load(System::Object^  sender, System::EventArgs^  e) {
		comboBox1->Items->Add("--- Todas as Universidades ---");
		comboBox1->SelectedIndex = 0;
		comboBox2->Enabled = FALSE;

		
		for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
			System::String^ text = gcnew String(((*it)->GetNome()).c_str());
			comboBox1->Items->Add(String::Format(text));
		}
}

	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		if (comboBox1->SelectedIndex != 0) {
			comboBox2->Enabled = TRUE;
			comboBox2->Items->Clear();
			comboBox2->Items->Add("--- Todas as Escolas ---");
			comboBox2->SelectedIndex = 0;
			std::string NomeUniv = msclr::interop::marshal_as<std::string>(comboBox1->Text);
			for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
				if (((*it)->GetNome()).c_str() == NomeUniv)
				{
					for (list<Escola*>::iterator itE = Escolas->begin(); itE != Escolas->end(); itE++) {
						if (((*it)->GetNome()).c_str() == (*itE)->GetUniv()->GetNome()) {
							System::String^ text = gcnew String(((*itE)->GetNome()).c_str());
							comboBox2->Items->Add(String::Format(text));
						}
					}
				}
			}
		} 
		else {
			comboBox2->Items->Clear();
			comboBox2->Enabled = FALSE;
		}
		dataGridView1->Rows->Clear();

		std::string NomeUniv = msclr::interop::marshal_as<std::string>(comboBox1->Text);
		for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++) {
			if (((Escola *)(*itD)->GetEsco())->GetUniv()->GetNome().c_str() == NomeUniv || comboBox1->SelectedIndex == 0)
			{
				dataGridView1->Rows->Add(
					gcnew Int32(Convert::ToInt32((*itD)->GetID())),
					gcnew String(((Escola *)(*itD)->GetEsco())->GetUniv()->GetNome().c_str()),
					gcnew String(((*itD)->GetEsco()->GetNome().c_str())),
					gcnew String(((*itD)->GetNome().c_str())),
					gcnew String(((*itD)->GetLog().c_str())),
					gcnew String(((*itD)->GetCod().c_str()))
				);
			}
		}
	}
	private: System::Void comboBox2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {


		dataGridView1->Rows->Clear();

		std::string NomeEsc = msclr::interop::marshal_as<std::string>(comboBox2->Text);
		for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++) {
			if (((*itD)->GetEsco()->GetNome()).c_str() == NomeEsc || comboBox2->SelectedIndex == 0)
			{
				dataGridView1->Rows->Add(
					gcnew Int32(Convert::ToInt32((*itD)->GetID())),
					gcnew String(((Escola *)(*itD)->GetEsco())->GetUniv()->GetNome().c_str()),
					gcnew String(((*itD)->GetEsco()->GetNome().c_str())),
					gcnew String(((*itD)->GetNome().c_str())),
					gcnew String(((*itD)->GetLog().c_str())),
					gcnew String(((*itD)->GetCod().c_str()))
				);
			}
		}
	}
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	this->Close();
}
};
}