#pragma once
#include <iostream>
#include "Cursos.h"
#include <list>
using namespace std;

class Disciplina
{
	string Codigo;
	string Nome;
	int valorEcts;
	Cursos *Curso;
//	list<Docente*> Doc;


public:
	Disciplina(Cursos *Curso, string Codigo, string Nome, int valorEcts);
	/*void AddCurso(Cursos* Cur);
	bool DelCurso(Cursos* CurD);*/
	Cursos* GetCursos() { return Curso; }
	int GetValorECTS() { return valorEcts; }
	string GetNome() { return Nome; }
	string GetCodigo() { return Codigo; }
	int GetMemoria() { return sizeof(*this); }
	void GravarXml(ofstream &File);
//	void AddDocente(Docente *Docent);
//	bool DelDocente(Docente *Docent);
	~Disciplina();
};

