#pragma once
#include "Pessoas.h"
#include "Disciplina.h"
#include <list>

class Docente :
	public Pessoas
{
	int QDisciplinas;
	int AnoInicio;
	Disciplina *Disc;
//	list<Disciplina*> Disc;
public:
	Docente(Disciplina *Disc, int ID, string Nome, string Genero, string Cidade, int QDiscplinas, int AnoInicio);
	int GetQDisci() { return QDisciplinas; }
	int GetAnoInicio() { return AnoInicio; }
	Disciplina *GetDisci() { return Disc; }
	//void GravarXml(ofstream &File);
//	void AddDisciplina(Disciplina *D);
//	bool DelDisciplina(Disciplina *D);
	~Docente();
};

