#include "Alunos.h"

Alunos::Alunos(int ID, string Nome, string Genero, string Cidade, int NMeca, double Media, Cursos *Cur)
	:Pessoas(ID, Nome, Genero, Cidade)
{
	Alunos::MediaEntrada = Media;
	Alunos::Cur = Cur;
	Alunos::NMecanografico = NMeca;
}

/*
void Alunos::MostrarAlunos()
{
	dataGridView1->Rows->Add(
		gcnew Int32(Convert::ToInt32((*itA)->GetID())),
		gcnew String(((*itA)->GetNome().c_str())),
		gcnew String(((*itA)->GetGenero().c_str())),
		gcnew String(((*itA)->GetCidade().c_str())),
		gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
		gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
		gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
		gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
		gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
	);
}*/






void Alunos::GravarXml(ofstream & File)
{
	File << "<Aluno>" << endl;
	File << "<Curso>" << GetCurso()->GetNome() << "</Curso>" << endl;
	File << "<ID>" << GetID() << "</ID>" << endl;
	File << "<Nome>" << GetNome() << "</Nome>" << endl;
	File << "<Genero>" << GetGenero() << "</Genero>" << endl;
	File << "<Cidade>" << GetCidade() << "</Cidade>" << endl;
	File << "<NMeca>" << GetNMeca() << "</NMeca>" << endl;
	File << "<Media>" << GetMedia() << "</Media>" << endl;
	File << "</Aluno>" << endl;
}

Alunos::~Alunos()
{
}



/*void Alunos::AddCurso(Cursos * Cur)
{
	/*Curs.push_back(Cur);
}

bool Alunos::DelCurso(Cursos * CurD)
{
	/*for (list<Cursos*>::iterator it = Curs.begin(); it != Curs.end(); it++)
	{
		if ((*it) == CurD) {
			Curs.erase(it);
			return true;
		}
	}
	return false;
}*/