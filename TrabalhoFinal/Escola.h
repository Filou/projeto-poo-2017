#pragma once
#include "Entidade.h"
#include "Universidade.h"
class Escola :
	public Entidade
{
	string Regiao;
	string Website;
	string Logo;
	Entidade *Univ;
public:
	Escola(int ID, Entidade *Univ, string Nome, string regiao, string codigo, string website, string logo);
	string GetReg() { return Regiao; };
	string GetWeb() { return Website; };
	string GetLogo() { return Logo; };
	int GetMemoria() { return sizeof(*this); };
	void GravarXml(ofstream &File);
	Entidade* GetUniv() { return Univ; }
	~Escola();
};

