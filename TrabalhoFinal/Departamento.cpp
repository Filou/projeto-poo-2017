#include "Departamento.h"


Departamento::Departamento(int ID, Entidade * Esco, string Nome, string logo, string codigo)
	:Entidade(ID, Nome, codigo)
{
	Departamento::Logo = logo;
	Departamento::Esco = Esco;
}

void Departamento::GravarXml(ofstream & File)
{
	File << "<Departamento>" << endl;
	File << "<idEscola>" << GetEsco()->GetID() << "</idEscola>" << endl;
	File << "<id>" << GetID() << "</id>" << endl;
	File << "<Nome>" << GetNome() << "</Nome>" << endl;
	File << "<Codigo>" << GetCod() << "</Codigo>" << endl;
	File << "<Logo>" << GetLog() << "</Logo>" << endl;
	File << "</Departamento>" << endl;
}

Departamento::~Departamento()
{
}
