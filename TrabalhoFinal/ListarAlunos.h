#pragma once
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
#include <string>

//Class
#include "Universidade.h"
#include "Escola.h"
#include "Departamento.h"
#include "Cursos.h"
#include "Alunos.h"

namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ListarAlunos
	/// </summary>
	public ref class ListarAlunos : public System::Windows::Forms::Form
	{
	public:
		list<Alunos *> *Alunoss;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	public:







	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ComboBox^  CBCurso;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ID;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeAlun;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Genero;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Cidade;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NMeca;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Media;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeUniv;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeEsco;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeCurso;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::Button^  button1;



















	public:
			 list<Cursos *> *Cursoss;
		ListarAlunos(list<Cursos *> *CS,list<Alunos *> *As)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Alunoss = As;
			Cursoss = CS;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ListarAlunos()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->ID = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeAlun = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Genero = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Cidade = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NMeca = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Media = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeUniv = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeEsco = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeCurso = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->CBCurso = (gcnew System::Windows::Forms::ComboBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(9) {
				this->ID, this->NomeAlun,
					this->Genero, this->Cidade, this->NMeca, this->Media, this->NomeUniv, this->NomeEsco, this->NomeCurso
			});
			this->dataGridView1->Location = System::Drawing::Point(6, 185);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->RowTemplate->Height = 24;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(1186, 389);
			this->dataGridView1->TabIndex = 7;
			// 
			// ID
			// 
			this->ID->HeaderText = L"ID";
			this->ID->Name = L"ID";
			this->ID->ReadOnly = true;
			// 
			// NomeAlun
			// 
			this->NomeAlun->HeaderText = L"Nome Aluno";
			this->NomeAlun->Name = L"NomeAlun";
			this->NomeAlun->ReadOnly = true;
			// 
			// Genero
			// 
			this->Genero->HeaderText = L"Genero";
			this->Genero->Name = L"Genero";
			this->Genero->ReadOnly = true;
			// 
			// Cidade
			// 
			this->Cidade->HeaderText = L"Cidade";
			this->Cidade->Name = L"Cidade";
			this->Cidade->ReadOnly = true;
			// 
			// NMeca
			// 
			this->NMeca->HeaderText = L"N� Mecanografico";
			this->NMeca->Name = L"NMeca";
			this->NMeca->ReadOnly = true;
			// 
			// Media
			// 
			this->Media->HeaderText = L"Media";
			this->Media->Name = L"Media";
			this->Media->ReadOnly = true;
			// 
			// NomeUniv
			// 
			this->NomeUniv->HeaderText = L"Universidade";
			this->NomeUniv->Name = L"NomeUniv";
			this->NomeUniv->ReadOnly = true;
			// 
			// NomeEsco
			// 
			this->NomeEsco->HeaderText = L"Escola";
			this->NomeEsco->Name = L"NomeEsco";
			this->NomeEsco->ReadOnly = true;
			// 
			// NomeCurso
			// 
			this->NomeCurso->HeaderText = L"Curso";
			this->NomeCurso->Name = L"NomeCurso";
			this->NomeCurso->ReadOnly = true;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->CBCurso);
			this->groupBox1->Controls->Add(this->textBox2);
			this->groupBox1->Controls->Add(this->textBox1);
			this->groupBox1->Controls->Add(this->dataGridView1);
			this->groupBox1->Location = System::Drawing::Point(26, 57);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(1208, 580);
			this->groupBox1->TabIndex = 8;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Listar Alunos";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(66, 110);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(226, 17);
			this->label3->TabIndex = 19;
			this->label3->Text = L"Listar Pelo Numero Mecanografico";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(66, 82);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(116, 17);
			this->label2->TabIndex = 18;
			this->label2->Text = L"Listar Pelo Nome";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(66, 52);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(110, 17);
			this->label1->TabIndex = 17;
			this->label1->Text = L"Listar Por Curso";
			// 
			// CBCurso
			// 
			this->CBCurso->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBCurso->FormattingEnabled = true;
			this->CBCurso->Location = System::Drawing::Point(320, 49);
			this->CBCurso->Name = L"CBCurso";
			this->CBCurso->Size = System::Drawing::Size(686, 24);
			this->CBCurso->TabIndex = 16;
			this->CBCurso->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarAlunos::CBCurso_SelectedIndexChanged);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(320, 107);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(686, 22);
			this->textBox2->TabIndex = 9;
			this->textBox2->TextChanged += gcnew System::EventHandler(this, &ListarAlunos::textBox2_TextChanged);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(320, 79);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(686, 22);
			this->textBox1->TabIndex = 8;
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &ListarAlunos::textBox1_TextChanged);
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1263, 28);
			this->menuStrip1->TabIndex = 9;
			this->menuStrip1->Text = L"menuStrip1";
			this->menuStrip1->ItemClicked += gcnew System::Windows::Forms::ToolStripItemClickedEventHandler(this, &ListarAlunos::menuStrip1_ItemClicked);
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->exitToolStripMenuItem->Size = System::Drawing::Size(161, 26);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &ListarAlunos::exitToolStripMenuItem_Click);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(471, 51);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 22);
			this->textBox3->TabIndex = 10;
			this->textBox3->TextChanged += gcnew System::EventHandler(this, &ListarAlunos::textBox3_TextChanged);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(416, 54);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(49, 17);
			this->label4->TabIndex = 12;
			this->label4->Text = L"Notas:";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(577, 54);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(41, 17);
			this->label5->TabIndex = 14;
			this->label5->Text = L"entre";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(624, 51);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(100, 22);
			this->textBox4->TabIndex = 13;
			this->textBox4->TextChanged += gcnew System::EventHandler(this, &ListarAlunos::textBox4_TextChanged);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button1);
			this->groupBox2->Controls->Add(this->textBox4);
			this->groupBox2->Controls->Add(this->label5);
			this->groupBox2->Controls->Add(this->textBox3);
			this->groupBox2->Controls->Add(this->label4);
			this->groupBox2->Location = System::Drawing::Point(26, 643);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(1208, 100);
			this->groupBox2->TabIndex = 15;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Pesquisas";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(730, 46);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(126, 31);
			this->button1->TabIndex = 15;
			this->button1->Text = L"Pesquisar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &ListarAlunos::button1_Click);
			// 
			// ListarAlunos
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1263, 757);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ListarAlunos";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"ListarAlunos";
			this->Load += gcnew System::EventHandler(this, &ListarAlunos::ListarAlunos_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void ListarAlunos_Load(System::Object^  sender, System::EventArgs^  e) {
	CBCurso->Items->Clear();
	CBCurso->Items->Add("--- Selecione um Curso ---");
	CBCurso->SelectedIndex = 0;
	for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
		System::String^ text = gcnew String(((*itC)->GetNome()).c_str());
		CBCurso->Items->Add(String::Format(text));
	}
	dataGridView1->Rows->Clear();
	for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++) {
		dataGridView1->Rows->Add(
			gcnew Int32(Convert::ToInt32((*itA)->GetID())),
			gcnew String(((*itA)->GetNome().c_str())),
			gcnew String(((*itA)->GetGenero().c_str())),
			gcnew String(((*itA)->GetCidade().c_str())),
			gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
			gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
			gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
			gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
			gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
		);
	}
}
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	std::string NomeAluno = msclr::interop::marshal_as<std::string>(textBox1->Text);
	dataGridView1->Rows->Clear();
	if (textBox1->Text == "")
	{
		for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++) {
				dataGridView1->Rows->Add(
					gcnew Int32(Convert::ToInt32((*itA)->GetID())),
					gcnew String(((*itA)->GetNome().c_str())),
					gcnew String(((*itA)->GetGenero().c_str())),
					gcnew String(((*itA)->GetCidade().c_str())),
					gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
					gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
					//gcnew String( ((Universidade *)(*itA)->GetCurso()->GetDepart()->GetEsco())->.c_str())
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
				);
		}
		
	}
	else
	{
		for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++) {
			System::String^ text = gcnew String(((*itA)->GetNome()).c_str());
			if ((*itA)->GetNome() == NomeAluno)
			{
				dataGridView1->Rows->Add(
					gcnew Int32(Convert::ToInt32((*itA)->GetID())),
					gcnew String(((*itA)->GetNome().c_str())),
					gcnew String(((*itA)->GetGenero().c_str())),
					gcnew String(((*itA)->GetCidade().c_str())),
					gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
					gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
				);
			}
		}
		if (CBCurso->SelectedIndex != 0) {
			CBCurso->SelectedIndex = 0;
		}
	}
	
	
}
private: System::Void CBCurso_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	std::string NomeCurso = msclr::interop::marshal_as<std::string>(CBCurso->Text);
	dataGridView1->Rows->Clear();
	if (textBox1->Text != "" && CBCurso->SelectedIndex != 0) {
		textBox1->Text = ""; 
		textBox2->Text = "";
		for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++) {
			System::String^ text = gcnew String(((*itA)->GetNome()).c_str());
			if ((*itA)->GetCurso()->GetNome() == NomeCurso)
			{
				dataGridView1->Rows->Add(
					gcnew Int32(Convert::ToInt32((*itA)->GetID())),
					gcnew String(((*itA)->GetNome().c_str())),
					gcnew String(((*itA)->GetGenero().c_str())),
					gcnew String(((*itA)->GetCidade().c_str())),
					gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
					gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
				);
			}
		}
	}
	else {
		for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++) {
			System::String^ text = gcnew String(((*itA)->GetNome()).c_str());
			if ((*itA)->GetCurso()->GetNome() == NomeCurso)
			{
				dataGridView1->Rows->Add(
					gcnew Int32(Convert::ToInt32((*itA)->GetID())),
					gcnew String(((*itA)->GetNome().c_str())),
					gcnew String(((*itA)->GetGenero().c_str())),
					gcnew String(((*itA)->GetCidade().c_str())),
					gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
					gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
				);
			}
		}
	}
		
}
private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	int NMeca = Int32::Parse(textBox2->Text);
	dataGridView1->Rows->Clear();
	if (textBox2->Text == "" && textBox1->Text == "")
	{
		for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++) {
			dataGridView1->Rows->Add(
				gcnew Int32(Convert::ToInt32((*itA)->GetID())),
				gcnew String(((*itA)->GetNome().c_str())),
				gcnew String(((*itA)->GetGenero().c_str())),
				gcnew String(((*itA)->GetCidade().c_str())),
				gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
				gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
				gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
				gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
				gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
			);
		}

	}
	else
	{
		textBox1->Text = "";
		for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++) {
			System::String^ text = gcnew String(((*itA)->GetNome()).c_str());
			if ((*itA)->GetNMeca() == NMeca)
			{
				dataGridView1->Rows->Add(
					gcnew Int32(Convert::ToInt32((*itA)->GetID())),
					gcnew String(((*itA)->GetNome().c_str())),
					gcnew String(((*itA)->GetGenero().c_str())),
					gcnew String(((*itA)->GetCidade().c_str())),
					gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
					gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
					gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
				);
			}
		}
		if (CBCurso->SelectedIndex != 0) {
			CBCurso->SelectedIndex = 0;
		}
	}

}
private: System::Void textBox3_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	
}
private: System::Void textBox4_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	int NotaMin = Int32::Parse(textBox3->Text);
	int NotaMax = Int32::Parse(textBox4->Text);
	dataGridView1->Rows->Clear();
	for (list<Alunos*>::iterator itA = Alunoss->begin(); itA != Alunoss->end(); itA++) {
		if (((*itA)->GetMedia()) >= NotaMin && ((*itA)->GetMedia()) <= NotaMax)
		{
			dataGridView1->Rows->Add(
				gcnew Int32(Convert::ToInt32((*itA)->GetID())),
				gcnew String(((*itA)->GetNome().c_str())),
				gcnew String(((*itA)->GetGenero().c_str())),
				gcnew String(((*itA)->GetCidade().c_str())),
				gcnew Int32(Convert::ToInt32((*itA)->GetNMeca())),
				gcnew Int32(Convert::ToInt32((*itA)->GetMedia())),
				gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
				gcnew String(((Escola *)((Cursos*)(*itA)->GetCurso())->GetDepart()->GetEsco())->GetCod().c_str()),
				gcnew String(((Cursos*)(*itA)->GetCurso())->GetNome().c_str())
			);
		}
		else if (NotaMin > NotaMax)
			MessageBox::Show("Inseriu uma nota minima maior que a nota min");
	}
}
private: System::Void menuStrip1_ItemClicked(System::Object^  sender, System::Windows::Forms::ToolStripItemClickedEventArgs^  e) {
}
};
}
