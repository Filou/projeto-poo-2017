#pragma once
#include "Pessoas.h"
#include "Cursos.h"
#include <list>
class Alunos :
	public Pessoas
{
	int MediaEntrada;
	int NMecanografico;
	Cursos *Cur;
	//int TotalAlunos = 0;
public:
	Alunos(int ID, string Nome, string Genero, string Cidade, int NMeca, double Media, Cursos *Cur);
	int GetMedia() { return MediaEntrada; }
	int GetNMeca() { return NMecanografico; }
	//int TotalAlunos() { return TotalAlunos; }
	//void MostrarAlunos();
	Cursos* GetCurso() { return Cur; }
	//void AddCurso(Cursos * Cur);
	//bool DelCurso(Cursos * CurD);
	int GetMemoria() { return sizeof(*this); }
	void GravarXml(ofstream &File);
	~Alunos();
};

