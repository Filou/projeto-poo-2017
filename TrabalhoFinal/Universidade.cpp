#include "Universidade.h"
#include "Escola.h"
using namespace System::Xml;

Universidade::Universidade(int ID, string Nome, string localidade, string codigo)
	:Entidade(ID,Nome, codigo)
{
	Universidade::Loc = localidade;
}


void Universidade::GravarXml(ofstream & File)
{

	/*XmlTextWriter ^writer = gcnew XmlTextWriter("Teste.xml", System::Text::Encoding::ASCII);
	writer->WriteStartDocument(true);
	writer->WriteStartElement("Lista");
	for (int i = 0; i < 3; i++)
	{
		writer->WriteStartElement("Produto");
		writer->WriteElementString("ID", "" + GetID());
		
		writer->WriteValue(GetNome().c_str());
		writer->WriteElementString("Descricao", "" + GetNome());
		writer->WriteElementString("Preco", "175.6");
		writer->WriteStartElement("URL");
		writer->WriteCData("http:\\\\www.test url&var=value");
		writer->WriteEndElement();
		writer->WriteEndElement();
	}
	writer->WriteEndDocument();
	writer->Flush();
	writer->Close();*/

	File << "<Univeridade>" << endl;

	File << "<id>" << GetID() << "</id>" << endl;
	File << "<Nome>" << GetNome().c_str() << "</Nome>" << endl;
	File << "<Localidade>" << GetLoc().c_str() << "</Localidade>" << endl;
	File << "<Codigo>" << GetCod().c_str() << "</Codigo>" << endl;
	/*for (list<Universidade*>::iterator itE = Universidades->begin(); itE != Universidades->end(); itE++)
	{
		(((Escola*)(*itE))->GravarXml(File));
	}*/
	File << "</Univeridade>" << endl;
}

/*void Universidade::LerXML(string ficheiro)
{
	list<string>ListaStrings;
	ifstream File;
	File.open(ficheiro);
	string S;
	int N = 0;
	Universidade *B;
	getline(File, S);
	while (!File.eof())
	{
		getline(File, S);
		ListaStrings.clear();
		Editar->PartirString(S, "<>", ' ', ListaStrings);
		if (ListaStrings.size() > N)
		{
			list<string>::iterator it = next(ListaStrings.begin(), N);
			if (*it == "Banco")
			{
				B = new Banco();
				B->LerXml(File);
				LBancos->push_back(B);
			}
		}


	}

	File.close();
}*/



Universidade::~Universidade()
{

}
