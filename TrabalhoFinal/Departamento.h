#pragma once
#include "Entidade.h"
#include "Escola.h"
class Departamento :
	public Entidade
{
	string Logo;
	Entidade *Esco;
public:
	Departamento(int ID, Entidade *Esco, string Nome, string logo, string codigo);
	string GetLog() { return Logo; }
	Entidade* GetEsco() { return Esco; }
	int GetID() { return Entidade::GetID(); }
	int GetMemoria() { return sizeof(*this); }
	void GravarXml(ofstream &File);
	~Departamento();
};

