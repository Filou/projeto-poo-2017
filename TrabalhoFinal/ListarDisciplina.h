#pragma once
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
#include <string>

//Class
#include "Universidade.h"
#include "Escola.h"
#include "Departamento.h"
#include "Cursos.h"
#include "Disciplina.h"

namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ListarDisciplina
	/// </summary>
	public ref class ListarDisciplina : public System::Windows::Forms::Form
	{
	public:
		list<Universidade *> *Universidades;
		list<Escola *> *Escolas;
		list<Departamento *> *Departamentos;
		list<Cursos *> *Cursoss;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeDisci;
	public:
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ECTS;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Codigo;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeUniv;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeEsco;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeDepart;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeCurso;
	public:
		list<Disciplina *> *Disciplinas;
		ListarDisciplina(list<Universidade *> *Us, list<Escola *> *ES, list<Departamento *> *DS, list<Cursos *> *CS, list<Disciplina *> *Disci)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Universidades = Us;
			Escolas = ES;
			Departamentos = DS;
			Cursoss = CS;
			Disciplinas = Disci;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ListarDisciplina()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected:
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::ComboBox^  CBCurso;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::ComboBox^  CBDepart;




	private: System::Windows::Forms::DataGridView^  dataGridView1;





	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ComboBox^  CBDisci;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ListarDisciplina::typeid));
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->CBDisci = (gcnew System::Windows::Forms::ComboBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->CBCurso = (gcnew System::Windows::Forms::ComboBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->CBDepart = (gcnew System::Windows::Forms::ComboBox());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->NomeDisci = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ECTS = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Codigo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeUniv = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeEsco = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeDepart = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeCurso = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->CBDisci);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->CBCurso);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->CBDepart);
			this->groupBox1->Controls->Add(this->dataGridView1);
			this->groupBox1->Location = System::Drawing::Point(38, 50);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(1219, 600);
			this->groupBox1->TabIndex = 9;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Listar Disciplinas";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(145, 155);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(132, 17);
			this->label1->TabIndex = 19;
			this->label1->Text = L"Listar por Disciplina";
			// 
			// CBDisci
			// 
			this->CBDisci->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBDisci->FormattingEnabled = true;
			this->CBDisci->Location = System::Drawing::Point(417, 152);
			this->CBDisci->Name = L"CBDisci";
			this->CBDisci->Size = System::Drawing::Size(686, 24);
			this->CBDisci->TabIndex = 18;
			this->CBDisci->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarDisciplina::CBDisci_SelectedIndexChanged);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(122, 116);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(1013, 17);
			this->label5->TabIndex = 17;
			this->label5->Text = resources->GetString(L"label5.Text");
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(145, 73);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(116, 17);
			this->label4->TabIndex = 16;
			this->label4->Text = L"Listar por Cursos";
			// 
			// CBCurso
			// 
			this->CBCurso->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBCurso->Enabled = false;
			this->CBCurso->FormattingEnabled = true;
			this->CBCurso->Location = System::Drawing::Point(417, 70);
			this->CBCurso->Name = L"CBCurso";
			this->CBCurso->Size = System::Drawing::Size(686, 24);
			this->CBCurso->TabIndex = 15;
			this->CBCurso->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarDisciplina::CBCurso_SelectedIndexChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(145, 43);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(98, 17);
			this->label3->TabIndex = 14;
			this->label3->Text = L"Departamento";
			// 
			// CBDepart
			// 
			this->CBDepart->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBDepart->FormattingEnabled = true;
			this->CBDepart->Location = System::Drawing::Point(417, 40);
			this->CBDepart->Name = L"CBDepart";
			this->CBDepart->Size = System::Drawing::Size(686, 24);
			this->CBDepart->TabIndex = 13;
			this->CBDepart->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarDisciplina::CBDepart_SelectedIndexChanged);
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(7) {
				this->NomeDisci,
					this->ECTS, this->Codigo, this->NomeUniv, this->NomeEsco, this->NomeDepart, this->NomeCurso
			});
			this->dataGridView1->Location = System::Drawing::Point(27, 205);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->RowTemplate->Height = 24;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(1186, 389);
			this->dataGridView1->TabIndex = 6;
			// 
			// NomeDisci
			// 
			this->NomeDisci->HeaderText = L"Nome Disciplina";
			this->NomeDisci->Name = L"NomeDisci";
			this->NomeDisci->ReadOnly = true;
			// 
			// ECTS
			// 
			this->ECTS->HeaderText = L"ECTS";
			this->ECTS->Name = L"ECTS";
			this->ECTS->ReadOnly = true;
			// 
			// Codigo
			// 
			this->Codigo->HeaderText = L"Codigo";
			this->Codigo->Name = L"Codigo";
			this->Codigo->ReadOnly = true;
			// 
			// NomeUniv
			// 
			this->NomeUniv->HeaderText = L"Nome Universidade";
			this->NomeUniv->Name = L"NomeUniv";
			this->NomeUniv->ReadOnly = true;
			// 
			// NomeEsco
			// 
			this->NomeEsco->HeaderText = L"Nome Escola";
			this->NomeEsco->Name = L"NomeEsco";
			this->NomeEsco->ReadOnly = true;
			// 
			// NomeDepart
			// 
			this->NomeDepart->HeaderText = L"Nome Departamento";
			this->NomeDepart->Name = L"NomeDepart";
			this->NomeDepart->ReadOnly = true;
			// 
			// NomeCurso
			// 
			this->NomeCurso->HeaderText = L"Nome Curso";
			this->NomeCurso->Name = L"NomeCurso";
			this->NomeCurso->ReadOnly = true;
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1269, 28);
			this->menuStrip1->TabIndex = 10;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->exitToolStripMenuItem->Size = System::Drawing::Size(161, 26);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &ListarDisciplina::exitToolStripMenuItem_Click);
			// 
			// ListarDisciplina
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1269, 680);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ListarDisciplina";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"ListarDisciplina";
			this->Load += gcnew System::EventHandler(this, &ListarDisciplina::ListarDisciplina_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
private: System::Void ListarDisciplina_Load(System::Object^  sender, System::EventArgs^  e) {
	CBDepart->Items->Clear();
	CBDepart->Items->Add("--- Selecione um Departamento ---");
	CBDepart->SelectedIndex = 0;

	for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++) {
		System::String^ text = gcnew String(((*itD)->GetNome()).c_str());
		CBDepart->Items->Add(String::Format(text));
	}
	CBDisci->Items->Clear();
	CBDisci->Items->Add("--- Todas as Disciplinas ---");
	CBDisci->SelectedIndex = 0;

	for (list<Disciplina*>::iterator itDi = Disciplinas->begin(); itDi != Disciplinas->end(); itDi++) {
		System::String^ text = gcnew String(((*itDi)->GetNome()).c_str());
		CBDisci->Items->Add(String::Format(text));
	}
}
private: System::Void CBDepart_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	if (CBDepart->SelectedIndex != 0) {
		CBCurso->Items->Clear();
		CBCurso->Items->Add("--- Selecione um Curso ---");
		CBCurso->SelectedIndex = 0;
		CBCurso->Enabled = TRUE;
		std::string NomeDepart = msclr::interop::marshal_as<std::string>(CBDepart->Text);
		for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
			System::String^ text = gcnew String(((*itC)->GetNome()).c_str());
			if((*itC)->GetDepart()->GetNome().c_str() == NomeDepart)
			CBCurso->Items->Add(String::Format(text));
		}
		dataGridView1->Rows->Clear();
		for (list<Disciplina*>::iterator itDisc = Disciplinas->begin(); itDisc != Disciplinas->end(); itDisc++) {
			if ((*itDisc)->GetCursos()->GetDepart()->GetNome().c_str() == NomeDepart)
			{
				dataGridView1->Rows->Add(
					gcnew String(((*itDisc)->GetNome().c_str())),
					gcnew Int32(Convert::ToInt32((*itDisc)->GetValorECTS())),
					gcnew String(((*itDisc)->GetCodigo().c_str())),
					gcnew String(((Escola*)((Departamento*)((Cursos*)(*itDisc)->GetCursos())->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str()),
					gcnew String(((Cursos*)(*itDisc)->GetCursos())->GetDepart()->GetEsco()->GetNome().c_str()),
					gcnew String(((Cursos*)(*itDisc)->GetCursos())->GetDepart()->GetNome().c_str()),
					gcnew String((*itDisc)->GetCursos()->GetNome().c_str())
					);
			}
		}
	}
	else
	{
		CBCurso->Enabled = FALSE;
		CBCurso->Items->Clear();
	}
}
private: System::Void CBCurso_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	std::string NomeCurso = msclr::interop::marshal_as<std::string>(CBDepart->Text);
	dataGridView1->Rows->Clear();
	for (list<Disciplina*>::iterator itDisc = Disciplinas->begin(); itDisc != Disciplinas->end(); itDisc++) {
		if ((*itDisc)->GetCursos()->GetDepart()->GetNome().c_str() == NomeCurso)
		{
			dataGridView1->Rows->Add(
				gcnew String(((*itDisc)->GetNome().c_str())),
				gcnew Int32(Convert::ToInt32((*itDisc)->GetValorECTS())),
				gcnew String(((*itDisc)->GetCodigo().c_str())),
				gcnew String(((Escola*)((Departamento*)((Cursos*)(*itDisc)->GetCursos())->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str()),
				gcnew String(((Cursos*)(*itDisc)->GetCursos())->GetDepart()->GetEsco()->GetNome().c_str()),
				gcnew String(((Cursos*)(*itDisc)->GetCursos())->GetDepart()->GetNome().c_str()),
				gcnew String((*itDisc)->GetCursos()->GetNome().c_str())
			);
		}
	}
}
private: System::Void CBDisci_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	
	if (CBDisci->SelectedIndex == 0) {
		if (CBDepart->SelectedIndex != 0) {
			CBDepart->SelectedIndex = 0;
		}
		else {
			dataGridView1->Rows->Clear();
			for (list<Disciplina*>::iterator itDisc = Disciplinas->begin(); itDisc != Disciplinas->end(); itDisc++) {
				dataGridView1->Rows->Add(
					gcnew String(((*itDisc)->GetNome().c_str())),
					gcnew Int32(Convert::ToInt32((*itDisc)->GetValorECTS())),
					gcnew String(((*itDisc)->GetCodigo().c_str())),
					gcnew String(((Escola*)((Departamento*)((Cursos*)(*itDisc)->GetCursos())->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str()),
					gcnew String(((Cursos*)(*itDisc)->GetCursos())->GetDepart()->GetEsco()->GetNome().c_str()),
					gcnew String(((Cursos*)(*itDisc)->GetCursos())->GetDepart()->GetNome().c_str()),
					gcnew String((*itDisc)->GetCursos()->GetNome().c_str())
				);
			}
		}
	}
	else
	{
		CBDepart->SelectedIndex = 0;
		std::string NomeDisci = msclr::interop::marshal_as<std::string>(CBDisci->Text);
		dataGridView1->Rows->Clear();
		for (list<Disciplina*>::iterator itDisc = Disciplinas->begin(); itDisc != Disciplinas->end(); itDisc++) {
			if ((*itDisc)->GetNome().c_str() == NomeDisci)
			{
				dataGridView1->Rows->Add(
					gcnew String(((*itDisc)->GetNome().c_str())),
					gcnew Int32(Convert::ToInt32((*itDisc)->GetValorECTS())),
					gcnew String(((*itDisc)->GetCodigo().c_str())),
					gcnew String(((Escola*)((Departamento*)((Cursos*)(*itDisc)->GetCursos())->GetDepart())->GetEsco())->GetUniv()->GetNome().c_str()),
					gcnew String(((Cursos*)(*itDisc)->GetCursos())->GetDepart()->GetEsco()->GetNome().c_str()),
					gcnew String(((Cursos*)(*itDisc)->GetCursos())->GetDepart()->GetNome().c_str()),
					gcnew String((*itDisc)->GetCursos()->GetNome().c_str())
				);
			}
		}
	}
}
};
}
