#include "Disciplina.h"


Disciplina::Disciplina(Cursos * Curso, string Codigo, string Nome, int valorEcts)
{
	Disciplina::Codigo = Codigo;
	Disciplina::Nome = Nome;
	Disciplina::valorEcts = valorEcts;
	Disciplina::Curso = Curso;
}

/*void Disciplina::AddCurso(Cursos * Cur)
{
	*Curso.push_back(Cur);
}

bool Disciplina::DelCurso(Cursos * CurD)
{
	for (list<Cursos*>::iterator it = Curso.begin(); it != Curso.end(); it++)
	{
		if ((*it) == CurD) {
			Curso.erase(it);
			return true;
		}
	}
	return false;
}*/

/*void Disciplina::AddDocente(Docente * Docent)
{
	Doc.push_back(Docent);
}

bool Disciplina::DelDocente(Docente * Docent)
{
	for (list<Docente*>::iterator it = Doc.begin(); it != Doc.end(); it++)
	{
		if ((*it) == Docent) {
			Doc.erase(it);
			return true;
		}
	}
	return false;
}
*/

void Disciplina::GravarXml(ofstream & File)
{
	File << "<Disciplina>" << endl;
	File << "<Curso>" << GetCursos()->GetNome() << "</Curso>" << endl;
	File << "<Nome>" << GetNome() << "</Nome>" << endl;
	File << "<ECTS>" << GetValorECTS() << "</ECTS>" << endl;
	File << "<Codigo>" << GetCodigo() << "</Codigo>" << endl;
	File << "</Disciplina>" << endl;
}

Disciplina::~Disciplina()
{
}
