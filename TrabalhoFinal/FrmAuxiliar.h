#pragma once
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
#include <string>

//Class
#include "Escola.h"
#include "Cursos.h"
#include "Alunos.h"
namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for FrmAuxiliar
	/// </summary>
	public ref class FrmAuxiliar : public System::Windows::Forms::Form
	{
	public:
		list<Escola *> *Escolas;

	private: System::Windows::Forms::ComboBox^  CBAlunos;
	public:


	public:
		list<Cursos *> *Cursoss;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	public:
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeCurso;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Codigo;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  TAlunos;
	public:
			 list<Alunos *> *Alunoss;
			 FrmAuxiliar(list<Cursos *> *CS, list<Escola *> *ES, list<Alunos *> *AS)
			 {
				 InitializeComponent();
				 //
				 //TODO: Add the constructor code here
				 //
				 Cursoss = CS;
				 Escolas = ES;
				 Alunoss = AS;
			 }

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FrmAuxiliar()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->NomeCurso = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Codigo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->TAlunos = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->CBAlunos = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->dataGridView1);
			this->groupBox1->Controls->Add(this->CBAlunos);
			this->groupBox1->Location = System::Drawing::Point(103, 75);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(1052, 377);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"groupBox1";
			this->groupBox1->Enter += gcnew System::EventHandler(this, &FrmAuxiliar::groupBox1_Enter);
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
				this->NomeCurso,
					this->Codigo, this->TAlunos
			});
			this->dataGridView1->Location = System::Drawing::Point(25, 124);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->RowTemplate->Height = 24;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(1008, 248);
			this->dataGridView1->TabIndex = 9;
			this->dataGridView1->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &FrmAuxiliar::dataGridView1_CellContentClick);
			// 
			// NomeCurso
			// 
			this->NomeCurso->HeaderText = L"Nome Curso";
			this->NomeCurso->Name = L"NomeCurso";
			this->NomeCurso->ReadOnly = true;
			// 
			// Codigo
			// 
			this->Codigo->HeaderText = L"Codigo";
			this->Codigo->Name = L"Codigo";
			this->Codigo->ReadOnly = true;
			// 
			// TAlunos
			// 
			this->TAlunos->HeaderText = L"Total Alunos";
			this->TAlunos->Name = L"TAlunos";
			this->TAlunos->ReadOnly = true;
			// 
			// CBAlunos
			// 
			this->CBAlunos->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBAlunos->FormattingEnabled = true;
			this->CBAlunos->Location = System::Drawing::Point(360, 52);
			this->CBAlunos->Name = L"CBAlunos";
			this->CBAlunos->Size = System::Drawing::Size(686, 24);
			this->CBAlunos->TabIndex = 8;
			this->CBAlunos->SelectedIndexChanged += gcnew System::EventHandler(this, &FrmAuxiliar::CBAlunos_SelectedIndexChanged);
			// 
			// FrmAuxiliar
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1255, 590);
			this->Controls->Add(this->groupBox1);
			this->Name = L"FrmAuxiliar";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"FrmAuxiliar";
			this->Load += gcnew System::EventHandler(this, &FrmAuxiliar::FrmAuxiliar_Load);
			this->groupBox1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void FrmAuxiliar_Load(System::Object^  sender, System::EventArgs^  e) {
		for (list<Alunos*>::iterator itC = Alunoss->begin(); itC != Alunoss->end(); itC++) {
			System::String^ text = gcnew String(((*itC)->GetCidade()).c_str());
			bool find = false;
			for (int i = 0; i < CBAlunos->Items->Count && !find; i++)
				if (CBAlunos->Items[i]->ToString() == text)
					find = !find;
			if (!find)
				CBAlunos->Items->Add(String::Format(text));
		}
	}
	private: System::Void CBAlunos_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		dataGridView1->Rows->Clear();
		for (list<Cursos*>::iterator itC = Cursoss->begin(); itC != Cursoss->end(); itC++) {
			int Countador = 0;
			list<Pessoas*> px = (*itC)->GetListAluno();
			for (list<Pessoas*>::iterator itP = px.begin(); itP != px.end(); itP++) {
				System::String^ text = gcnew String(((*itP)->GetCidade()).c_str());
				if (CBAlunos->Items[CBAlunos->SelectedIndex]->ToString() == text)
					Countador++;
			}
			
			dataGridView1->Rows->Add(
				gcnew String(((*itC)->GetNome().c_str())),
				gcnew String(((*itC)->GetCod().c_str())),
				gcnew Int32(Convert::ToInt32(Countador))
			);
		}
		dataGridView1->Sort(dataGridView1->Columns["TAlunos"], ListSortDirection::Descending);
	}
private: System::Void groupBox1_Enter(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void dataGridView1_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}
};
}
