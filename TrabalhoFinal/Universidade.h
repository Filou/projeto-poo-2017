#pragma once
#include "Entidade.h"
#include "Escola.h"
#include <iostream>
#include <fstream>
#include <list>
#include <string>

class Universidade :
	Entidade
{
	string Loc;
	list<Universidade *>*Universidades;
public:
	Universidade(int IDUni, string Nome, string localidade, string codigo);
	string GetNome() { return Entidade::GetNome(); };
	string GetCod() { return Entidade::GetCod(); };
	string GetLoc() { return Loc; }
	int GetMemoria() { return sizeof(*this); }
	void GravarXml(ofstream &File);
	/*void LerXML(string ficheiro);*/
	int GetID() { return Entidade::GetID(); }
	~Universidade();
};

