#pragma once
#include "Universidade.h"
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ApagarUniversidade
	/// </summary>
	public ref class ApagarUniversidade : public System::Windows::Forms::Form
	{
	public:
		list<Universidade *> *Universidades;
	private: System::Windows::Forms::Label^  label1;
	public:
		list<Escola *> *Escolas;
		ApagarUniversidade(list<Universidade *> *Us, list<Escola *> *ES)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Universidades = Us;
			Escolas = ES;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ApagarUniversidade()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ID;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Codigo;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Nome;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Localidade;
	private: System::Windows::Forms::ComboBox^  comboBox1;




	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->ID = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Codigo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Nome = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Localidade = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(880, 504);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(123, 38);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Apagar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &ApagarUniversidade::button1_Click);
			// 
			// button2
			// 
			this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button2->Location = System::Drawing::Point(1009, 504);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(123, 38);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Fechar";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &ApagarUniversidade::button2_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Controls->Add(this->dataGridView1);
			this->groupBox1->Controls->Add(this->button2);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Location = System::Drawing::Point(78, 36);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(1151, 548);
			this->groupBox1->TabIndex = 3;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Universidade";
			this->groupBox1->Enter += gcnew System::EventHandler(this, &ApagarUniversidade::Univeridades_Enter);
			// 
			// comboBox1
			// 
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(442, 407);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(690, 24);
			this->comboBox1->TabIndex = 5;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &ApagarUniversidade::comboBox1_SelectedIndexChanged);
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
				this->ID, this->Codigo,
					this->Nome, this->Localidade
			});
			this->dataGridView1->Location = System::Drawing::Point(6, 44);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->RowTemplate->Height = 24;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(1126, 357);
			this->dataGridView1->TabIndex = 4;
			// 
			// ID
			// 
			this->ID->HeaderText = L"Identificador";
			this->ID->Name = L"ID";
			this->ID->ReadOnly = true;
			// 
			// Codigo
			// 
			this->Codigo->HeaderText = L"Codigo";
			this->Codigo->Name = L"Codigo";
			this->Codigo->ReadOnly = true;
			// 
			// Nome
			// 
			this->Nome->HeaderText = L"Nome Universidade";
			this->Nome->Name = L"Nome";
			this->Nome->ReadOnly = true;
			// 
			// Localidade
			// 
			this->Localidade->HeaderText = L"Localidade";
			this->Localidade->Name = L"Localidade";
			this->Localidade->ReadOnly = true;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(142, 410);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(298, 17);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Selecione a universidade que deseja eliminar:";
			// 
			// ApagarUniversidade
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1272, 640);
			this->Controls->Add(this->groupBox1);
			this->Name = L"ApagarUniversidade";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"ApagarUniversidade";
			this->Load += gcnew System::EventHandler(this, &ApagarUniversidade::ApagarUniversidade_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
	private: System::Void ApagarUniversidade_Load(System::Object^  sender, System::EventArgs^  e) {
		comboBox1->Items->Clear();
		comboBox1->Items->Add("--- Selecione a universidade ---");
		comboBox1->SelectedIndex = 0;
		for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
			System::String^ text = gcnew String(((*it)->GetNome()).c_str());
			comboBox1->Items->Add(String::Format(text));
			dataGridView1->Rows->Add(
				gcnew Int32(Convert::ToInt32((*it)->GetID())),
				gcnew String(((*it)->GetNome().c_str())),
				gcnew String(((*it)->GetLoc().c_str())),
				gcnew String(((*it)->GetCod().c_str()))
			);
		}
	}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	//dataGridView1->SelectedRows->Clear();
	Escola *C;
	
	std::string NomeUniv = msclr::interop::marshal_as<std::string>(comboBox1->Text);
	for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
		if (NomeUniv == (*it)->GetNome().c_str() && Escolas->empty())
			{
				Universidades->erase(it);
				dataGridView1->Rows->Clear();
				for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
					System::String^ text = gcnew String(((*it)->GetNome()).c_str());
					comboBox1->Items->Add(String::Format(text));
					dataGridView1->Rows->Add(
						gcnew Int32(Convert::ToInt32((*it)->GetID())),
						gcnew String(((*it)->GetNome().c_str())),
						gcnew String(((*it)->GetLoc().c_str())),
						gcnew String(((*it)->GetCod().c_str()))
					);
				}
				return;
			}	
		else
		{
			MessageBox::Show("Apague as escolas associadas a universidade: " + comboBox1->Text + ".");
		}
	}
}
private: System::Void Univeridades_Enter(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
}
};
}
