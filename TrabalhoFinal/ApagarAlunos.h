#pragma once
#include "Cursos.h"
#include "Alunos.h"
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ApagarAlunos
	/// </summary>
	public ref class ApagarAlunos : public System::Windows::Forms::Form
	{
	public:
		list<Cursos *> *Cursoss;
		list<Alunos *> *Alunoss;
		ApagarAlunos(list<Cursos *> *Cur, list<Alunos *> *Alun)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Cursoss = Cur;
			Alunoss = Alun;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ApagarAlunos()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected:
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::DataGridView^  dataGridView1;




	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Nmeca;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeA;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Curso;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->Nmeca = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeA = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Curso = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Controls->Add(this->dataGridView1);
			this->groupBox1->Controls->Add(this->button2);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Location = System::Drawing::Point(44, 83);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(1151, 548);
			this->groupBox1->TabIndex = 4;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Universidade";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(676, 410);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(198, 17);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Selecione o N� Mecanogr�fico";
			// 
			// comboBox1
			// 
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(880, 407);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(252, 24);
			this->comboBox1->TabIndex = 5;
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
				this->Nmeca,
					this->NomeA, this->Curso
			});
			this->dataGridView1->Location = System::Drawing::Point(6, 44);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->RowTemplate->Height = 24;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(1126, 357);
			this->dataGridView1->TabIndex = 4;
			// 
			// Nmeca
			// 
			this->Nmeca->HeaderText = L"N� Mecanogr�fico";
			this->Nmeca->Name = L"Nmeca";
			this->Nmeca->ReadOnly = true;
			// 
			// NomeA
			// 
			this->NomeA->HeaderText = L"Nome Aluno";
			this->NomeA->Name = L"NomeA";
			this->NomeA->ReadOnly = true;
			// 
			// Curso
			// 
			this->Curso->HeaderText = L"Curso";
			this->Curso->Name = L"Curso";
			this->Curso->ReadOnly = true;
			// 
			// button2
			// 
			this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button2->Location = System::Drawing::Point(1009, 504);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(123, 38);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Fechar";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &ApagarAlunos::button2_Click);
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(880, 504);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(123, 38);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Apagar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &ApagarAlunos::button1_Click);
			// 
			// ApagarAlunos
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1253, 659);
			this->Controls->Add(this->groupBox1);
			this->Name = L"ApagarAlunos";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"ApagarAlunos";
			this->Load += gcnew System::EventHandler(this, &ApagarAlunos::ApagarAlunos_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		//dataGridView1->SelectedRows->Clear();
		Cursos *C;
		
		if (comboBox1->SelectedIndex == 0)
		{
			MessageBox::Show("Nenhum aluno selecionado: " + comboBox1->Text + ".");
			return;
		}
		int NMeca = Int32::Parse(comboBox1->Text);
		for (list<Alunos*>::iterator it = Alunoss->begin(); it != Alunoss->end(); it++) {
			if (NMeca == (*it)->GetNMeca())
			{
	
				String^ Texto1 = gcnew String((*it)->GetNome().c_str());
				int Texto2 = (*it)->GetNMeca();
				C = (*it)->GetCurso();
				(*it)->GetCurso()->DelAluno(*it);
				Alunoss->erase(it);
				
				dataGridView1->Rows->Clear();
				for (list<Alunos*>::iterator it = Alunoss->begin(); it != Alunoss->end(); it++) {
					int NMeca = (*it)->GetNMeca();
					comboBox1->Items->Add(NMeca);
					dataGridView1->Rows->Add(
						gcnew Int32(Convert::ToInt32((*it)->GetNMeca())),
						gcnew String(((*it)->GetNome().c_str())),
						gcnew String(((*it)->GetCurso()->GetNome().c_str() ))
					);
				}
				MessageBox::Show("Apagou o aluno: " + Texto1 + " n�: " + Texto2 + ".");
				return;
			}
		}
	}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
private: System::Void ApagarAlunos_Load(System::Object^  sender, System::EventArgs^  e) {
	comboBox1->Items->Clear();
	comboBox1->Items->Add("--- Selecione um aluno ---");
	comboBox1->SelectedIndex = 0;
	for (list<Alunos*>::iterator it = Alunoss->begin(); it != Alunoss->end(); it++) {
		int NMeca = (*it)->GetNMeca();
		comboBox1->Items->Add(NMeca);
		dataGridView1->Rows->Add(
			gcnew Int32(Convert::ToInt32((*it)->GetNMeca())),
			gcnew String(((*it)->GetNome().c_str())),
			gcnew String(((*it)->GetCurso()->GetNome().c_str()))
		);
	}
}
};
}
