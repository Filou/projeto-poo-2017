#pragma once
#include "Escola.h"
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
#include <string>
namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ListarEscolas
	/// </summary>
	public ref class ListarEscolas : public System::Windows::Forms::Form
	{
	public:
		list<Escola *> *Escolas;

	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ID;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeUniv;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  NomeEsc;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Regiao;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Codigo;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Website;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Logo;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;







	public:
		list<Universidade *> *Universidades;
		ListarEscolas(list<Escola *> *Es, list<Universidade *> *Us)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Escolas = Es;
			Universidades = Us;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ListarEscolas()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ComboBox^  CBLEscola;
	protected:


	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->CBLEscola = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->ID = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeUniv = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->NomeEsc = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Regiao = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Codigo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Website = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Logo = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// CBLEscola
			// 
			this->CBLEscola->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->CBLEscola->FormattingEnabled = true;
			this->CBLEscola->Location = System::Drawing::Point(168, 57);
			this->CBLEscola->Name = L"CBLEscola";
			this->CBLEscola->Size = System::Drawing::Size(761, 24);
			this->CBLEscola->TabIndex = 1;
			this->CBLEscola->SelectedIndexChanged += gcnew System::EventHandler(this, &ListarEscolas::comboBox2_SelectedIndexChanged);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->dataGridView1);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->CBLEscola);
			this->groupBox1->Location = System::Drawing::Point(52, 57);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(1101, 640);
			this->groupBox1->TabIndex = 3;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Listar Escolas";
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->AllowUserToResizeColumns = false;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(7) {
				this->ID, this->NomeUniv,
					this->NomeEsc, this->Regiao, this->Codigo, this->Website, this->Logo
			});
			this->dataGridView1->Location = System::Drawing::Point(20, 107);
			this->dataGridView1->MultiSelect = false;
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersVisible = false;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->RowTemplate->Height = 24;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(1060, 514);
			this->dataGridView1->TabIndex = 4;
			// 
			// ID
			// 
			this->ID->HeaderText = L"Identificador";
			this->ID->Name = L"ID";
			this->ID->ReadOnly = true;
			// 
			// NomeUniv
			// 
			this->NomeUniv->HeaderText = L"Nome Universidade";
			this->NomeUniv->Name = L"NomeUniv";
			this->NomeUniv->ReadOnly = true;
			// 
			// NomeEsc
			// 
			this->NomeEsc->HeaderText = L"Nome Escola";
			this->NomeEsc->Name = L"NomeEsc";
			this->NomeEsc->ReadOnly = true;
			// 
			// Regiao
			// 
			this->Regiao->HeaderText = L"Regiao";
			this->Regiao->Name = L"Regiao";
			this->Regiao->ReadOnly = true;
			// 
			// Codigo
			// 
			this->Codigo->HeaderText = L"Codigo";
			this->Codigo->Name = L"Codigo";
			this->Codigo->ReadOnly = true;
			// 
			// Website
			// 
			this->Website->HeaderText = L"Website";
			this->Website->Name = L"Website";
			this->Website->ReadOnly = true;
			// 
			// Logo
			// 
			this->Logo->HeaderText = L"Logo";
			this->Logo->Name = L"Logo";
			this->Logo->ReadOnly = true;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(17, 60);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(91, 17);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Universidade";
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1210, 28);
			this->menuStrip1->TabIndex = 4;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->exitToolStripMenuItem->Size = System::Drawing::Size(161, 26);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &ListarEscolas::exitToolStripMenuItem_Click);
			// 
			// ListarEscolas
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1210, 709);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ListarEscolas";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"ListarEscolas";
			this->Load += gcnew System::EventHandler(this, &ListarEscolas::ListarEscolas_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ListarEscolas_Load(System::Object^  sender, System::EventArgs^  e) {
		CBLEscola->Items->Add("--- Todas as universidades ---");
		CBLEscola->SelectedIndex = 0;
		for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
			System::String^ text = gcnew String(((*it)->GetNome()).c_str());
			CBLEscola->Items->Add(String::Format(text));
		}
	}
	private: System::Void listBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	
	}
	private: System::Void comboBox2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		dataGridView1->Rows->Clear();
		std::string NomeUniv = msclr::interop::marshal_as<std::string>(CBLEscola->Text);
				for (list<Escola*>::iterator itE = Escolas->begin(); itE != Escolas->end(); itE++) {
					if ( (*itE)->GetUniv()->GetNome() == NomeUniv || CBLEscola->SelectedIndex == 0) {
						dataGridView1->Rows->Add(
							"",
							gcnew String(((*itE)->GetUniv()->GetNome().c_str())),
							gcnew String(((*itE)->GetNome().c_str())),
							gcnew String(((*itE)->GetReg().c_str())),
							gcnew String(((*itE)->GetCod().c_str())),
							gcnew String(((*itE)->GetWeb().c_str())), 
							gcnew String(((*itE)->GetLogo().c_str()))

						);
					}
				}
	}
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
};
}
