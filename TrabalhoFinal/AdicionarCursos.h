#pragma once
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
#include <string>

//Class
#include "Universidade.h"
#include "Escola.h"
#include "Departamento.h"
#include "Cursos.h"

namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for AdicionarCursos
	/// </summary>
	public ref class AdicionarCursos : public System::Windows::Forms::Form
	{
	public:
		list<Universidade *> *Universidades;
		list<Escola *> *Escolas;
		list<Departamento *> *Departamentos;
		list<Cursos *> *Cursoss;
		//DAtual = std::string Nome = msclr::interop::marshal_as<std::string>(textBox3->Text);

		AdicionarCursos(list<Universidade *> *Us, list<Escola *> *ES, list<Departamento *> *DS, list<Cursos *> *CS)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Universidades = Us;
			Escolas = ES;
			Departamentos = DS;
			Cursoss = CS;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~AdicionarCursos()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected:
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::GroupBox^  groupBox1;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ComboBox^  comboBox1;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::IO::FileSystemWatcher^  fileSystemWatcher1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  Adicionar;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->Adicionar = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->fileSystemWatcher1 = (gcnew System::IO::FileSystemWatcher());
			this->menuStrip1->SuspendLayout();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fileSystemWatcher1))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1053, 28);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->exitToolStripMenuItem->Size = System::Drawing::Size(161, 26);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &AdicionarCursos::exitToolStripMenuItem_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Controls->Add(this->textBox2);
			this->groupBox1->Controls->Add(this->Adicionar);
			this->groupBox1->Controls->Add(this->textBox1);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Location = System::Drawing::Point(56, 57);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(876, 262);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Adicionar Curso";
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(697, 196);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(123, 38);
			this->button1->TabIndex = 12;
			this->button1->Text = L"Limpar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &AdicionarCursos::button1_Click);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(287, 127);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(533, 22);
			this->textBox2->TabIndex = 5;
			// 
			// Adicionar
			// 
			this->Adicionar->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->Adicionar->Location = System::Drawing::Point(569, 196);
			this->Adicionar->Name = L"Adicionar";
			this->Adicionar->Size = System::Drawing::Size(123, 38);
			this->Adicionar->TabIndex = 11;
			this->Adicionar->Text = L"Addcionar";
			this->Adicionar->UseVisualStyleBackColor = true;
			this->Adicionar->Click += gcnew System::EventHandler(this, &AdicionarCursos::Adicionar_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(287, 91);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(533, 22);
			this->textBox1->TabIndex = 4;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(45, 130);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(52, 17);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Codigo";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(45, 94);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(86, 17);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Nome Curso";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(45, 57);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(98, 17);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Departamento";
			// 
			// comboBox1
			// 
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(287, 54);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(533, 24);
			this->comboBox1->TabIndex = 0;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &AdicionarCursos::comboBox1_SelectedIndexChanged);
			// 
			// fileSystemWatcher1
			// 
			this->fileSystemWatcher1->EnableRaisingEvents = true;
			this->fileSystemWatcher1->SynchronizingObject = this;
			// 
			// AdicionarCursos
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1053, 376);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"AdicionarCursos";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"AdicionarCursos";
			this->Load += gcnew System::EventHandler(this, &AdicionarCursos::AdicionarCursos_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fileSystemWatcher1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	comboBox1->SelectedIndex = 0;
	textBox1->Text = "";
	textBox2->Text = "";
}
private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	
}

private: System::Void Adicionar_Click(System::Object^  sender, System::EventArgs^  e) {
	if (MessageBox::Show(
		"Deseja adicionar:\n\nNome Departamento: [" + comboBox1->Text + "]\n" +
		"Nome Curso: [" + textBox1->Text + "]\n" +
		"Codigo: [" + textBox2->Text + "]\n",
		"Deseja Adicionar?", MessageBoxButtons::YesNo,
		MessageBoxIcon::Question) == ::System::Windows::Forms::DialogResult::Yes)
	{
		std::string Nome = msclr::interop::marshal_as<std::string>(textBox1->Text);
		std::string Codigo = msclr::interop::marshal_as<std::string>(textBox2->Text);
		std::string NomeCurso = msclr::interop::marshal_as<std::string>(comboBox1->Text);

		Departamento *D;

		for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++) {
			if (((*itD)->GetNome()).c_str() == NomeCurso)
			{
				D = (*itD);
				break;
			}
		}
		Cursos *C = new Cursos(Codigo, Nome, D);
		Cursoss->push_back(C);
		this->Close();
	}
}
private: System::Void AdicionarCursos_Load(System::Object^  sender, System::EventArgs^  e) {
	comboBox1->Items->Clear();
	comboBox1->Items->Add("--- Selecione um Departamento ---");
	comboBox1->SelectedIndex = 0;
	for (list<Departamento*>::iterator itD = Departamentos->begin(); itD != Departamentos->end(); itD++) {
		System::String^ text = gcnew String(((*itD)->GetNome()).c_str());
		comboBox1->Items->Add(String::Format(text));
	}
}
};
}
