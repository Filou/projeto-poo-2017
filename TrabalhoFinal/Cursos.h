#pragma once
#include <iostream>
#include "Departamento.h"
#include "Pessoas.h"
#include <list>

using namespace std;
class Cursos
{

	string Codigo;
	string Nome;
	Departamento *Depart;
	list<Pessoas*> Pess;
	int NAlunos;
public:
	Cursos(string Codigo, string Nome, Departamento *Depart);
	string GetNome() { return Nome; }
	string GetCod() { return Codigo; }
	Departamento* GetDepart() { return Depart; }
	void AddAluno(Pessoas *P);
	list<Pessoas *> GetListAluno() { return Pess; }
	bool DelAluno(Pessoas *P);
	int GetNAlunos() { return Pess.size(); }
	int GetMemoria() { return sizeof(*this); }
	void GravarXml(ofstream &File);
	~Cursos();
};

