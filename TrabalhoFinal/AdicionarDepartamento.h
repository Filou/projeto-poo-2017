#pragma once
#include <list>
#include <msclr\marshal_cppstd.h>
#include <cstring>
#include <string>

//Class
#include "Universidade.h"
#include "Escola.h"
#include "Departamento.h"



namespace TrabalhoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for AdicionarDepartamento
	/// </summary>
	public ref class AdicionarDepartamento : public System::Windows::Forms::Form
	{
	public:
		list<Universidade *> *Universidades;
		list<Escola *> *Escolas;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	public:
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;

	public:


			 list<Departamento *> *Departamentos;
		AdicionarDepartamento(list<Universidade *> *Us, list<Escola *> *ES, list<Departamento *> *DS)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Universidades = Us;
			Escolas = ES;
			Departamentos = DS;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~AdicionarDepartamento()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected:
	private: System::Windows::Forms::ComboBox^  comboBox2;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  Adicionar;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->Adicionar = (gcnew System::Windows::Forms::Button());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->groupBox1->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Controls->Add(this->Adicionar);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->textBox5);
			this->groupBox1->Controls->Add(this->textBox4);
			this->groupBox1->Controls->Add(this->textBox3);
			this->groupBox1->Controls->Add(this->textBox1);
			this->groupBox1->Controls->Add(this->comboBox2);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(57, 60);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(991, 276);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Departamento";
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(737, 232);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(123, 38);
			this->button1->TabIndex = 15;
			this->button1->Text = L"Apagar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &AdicionarDepartamento::button1_Click);
			// 
			// Adicionar
			// 
			this->Adicionar->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->Adicionar->Location = System::Drawing::Point(608, 232);
			this->Adicionar->Name = L"Adicionar";
			this->Adicionar->Size = System::Drawing::Size(123, 38);
			this->Adicionar->TabIndex = 14;
			this->Adicionar->Text = L"Addcionar";
			this->Adicionar->UseVisualStyleBackColor = true;
			this->Adicionar->Click += gcnew System::EventHandler(this, &AdicionarDepartamento::Adicionar_Click);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(131, 191);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(40, 17);
			this->label7->TabIndex = 13;
			this->label7->Text = L"Logo";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(131, 163);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(52, 17);
			this->label6->TabIndex = 12;
			this->label6->Text = L"Codigo";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(131, 137);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(45, 17);
			this->label5->TabIndex = 11;
			this->label5->Text = L"Nome";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(131, 107);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(21, 17);
			this->label3->TabIndex = 9;
			this->label3->Text = L"ID";
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(284, 188);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(576, 22);
			this->textBox5->TabIndex = 8;
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(284, 160);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(576, 22);
			this->textBox4->TabIndex = 7;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(284, 132);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(576, 22);
			this->textBox3->TabIndex = 6;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(284, 104);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(576, 22);
			this->textBox1->TabIndex = 4;
			// 
			// comboBox2
			// 
			this->comboBox2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Location = System::Drawing::Point(284, 73);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(576, 24);
			this->comboBox2->TabIndex = 3;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(131, 76);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(50, 17);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Escola";
			// 
			// comboBox1
			// 
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(284, 43);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(576, 24);
			this->comboBox1->TabIndex = 1;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &AdicionarDepartamento::comboBox1_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(131, 46);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(91, 17);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Universidade";
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1132, 28);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(44, 24);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->exitToolStripMenuItem->Size = System::Drawing::Size(161, 26);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &AdicionarDepartamento::exitToolStripMenuItem_Click_1);
			// 
			// AdicionarDepartamento
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1132, 407);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"AdicionarDepartamento";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"AdicionarDepartamento";
			this->Load += gcnew System::EventHandler(this, &AdicionarDepartamento::AdicionarDepartamento_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Adicionar_Click(System::Object^  sender, System::EventArgs^  e) {
		if (MessageBox::Show(
			"Deseja adicionar:\n\nNome Universidade: [" + comboBox1->Text + "]\n" +
			"Nome Escola: [" + comboBox2->Text + "]\n" +
			"ID: [" + textBox1->Text + "]\n" +
			"Nome Departamento: [" + textBox3->Text + "]\n" +
			"Codigo: [" + textBox4->Text + "]\n" +
			"Logo: [" + textBox5->Text + "]\n",
			"Deseja Adicionar?", MessageBoxButtons::YesNo,
			MessageBoxIcon::Question) == ::System::Windows::Forms::DialogResult::Yes)
		{
			int intVar = Int32::Parse(textBox1->Text);
			std::string Nome = msclr::interop::marshal_as<std::string>(textBox3->Text);
			std::string Codigo = msclr::interop::marshal_as<std::string>(textBox4->Text);
			std::string Logo = msclr::interop::marshal_as<std::string>(textBox5->Text);
			std::string NomeUniv = msclr::interop::marshal_as<std::string>(comboBox1->Text);
			std::string NomeEsc = msclr::interop::marshal_as<std::string>(comboBox2->Text);
			Entidade *E;

			for (list<Escola*>::iterator it = Escolas->begin(); it != Escolas->end(); it++) {
				if (((*it)->GetNome()).c_str() == NomeEsc)
				{
					E = (Entidade *)(*it);
					break;
				}
			}
			Departamento *D = new Departamento(intVar, E, Nome, Logo, Codigo);
			Departamentos->push_back(D);
			this->Close();
		}

	}
private: System::Void AdicionarDepartamento_Load(System::Object^  sender, System::EventArgs^  e) {
	comboBox1->Items->Add("--- Selecione uma Universidade ---");
	comboBox1->SelectedIndex = 0;
	comboBox2->Items->Add("--- Selecione uma Escola ---");
	comboBox2->SelectedIndex = 0;
	comboBox2->Enabled = FALSE;
	for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
		System::String^ NomeUni = gcnew String(((*it)->GetNome()).c_str());
		comboBox1->Items->Add(String::Format(NomeUni));
	}
	
	
}

private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	
	std::string NomeUniv = msclr::interop::marshal_as<std::string>(comboBox1->Text);
	if (comboBox1->SelectedIndex != 0 && comboBox1->Text != "") {
		comboBox2->Items->Clear();
		comboBox2->Items->Add("--- Selecione uma Escola ---");
		comboBox2->SelectedIndex = 0;
		comboBox2->Enabled = TRUE;
		for (list<Universidade*>::iterator it = Universidades->begin(); it != Universidades->end(); it++) {
			if (((*it)->GetNome()).c_str() == NomeUniv)
			{
				for (list<Escola*>::iterator itE = Escolas->begin(); itE != Escolas->end(); itE++) {
					if (((*it)->GetNome()).c_str() == (*itE)->GetUniv()->GetNome()) {
						System::String^ text = gcnew String(((*itE)->GetNome()).c_str());
						comboBox2->Items->Add(String::Format(text));
					}
				}
			}
		}
	}
	else {
		comboBox2->Enabled = FALSE; comboBox1->SelectedIndex = 0; 
	}
}
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	
}
private: System::Void exitToolStripMenuItem_Click_1(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	comboBox1->SelectedIndex = 0;
	comboBox2->SelectedIndex = 0;
	textBox1->Text = "";
	textBox3->Text = "";
	textBox4->Text = "";
	textBox5->Text = "";
}
};
}
